<?php
/**************************************************************************

  ##\      ##\                     ##\        ######\            ##\ 
  ###\    ### |                    ## |      ##  __##\           ## |
  ####\  #### | ######\   #######\ #######\  ## /  \__| ######\  ## |
  ##\##\## ## |##  __##\ ##  _____|##  __##\ ## |       \____##\ ## |
  ## \###  ## |######## |\######\  ## |  ## |## |       ####### |## |
  ## |\#  /## |##   ____| \____##\ ## |  ## |## |  ##\ ##  __## |## |
  ## | \_/ ## |\#######\ #######  |## |  ## |\######  |\####### |## |
  \__|     \__| \_______|\_______/ \__|  \__| \______/  \_______|\__| 00.24

**************************************************************************/

// https://meshcal.net

// CC BY SA - Jérôme Leignadier-Paradon (2015)
// Creative Commons 4.0
// http://creativecommons.org/licenses/by-sa/4.0/


/*	Afero GPL v3
	MeshCal - Calendar server (whis rfc 2445 and 5545)
    Copyright (C) 2015 Jérôme Leignadier-Paradon

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see :
	https://www.gnu.org/licenses/agpl.html
*/

// CeCILL 2.1 - Jérôme Leignadier-Paradon (2015)
// http://www.cecill.info/faq.fr.html

//-------------------------------------------------------------------------





/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*     verouille cetaines fonctionalité pour facilité le dévelopement    */
/*     à passer en "false" avant de metre en prod !!!                    */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
$developement=false; 


if($developement)$temps_de_traitement=microtime(true); //pour tester la rapidité du code...



include "res/_config_.php"; // toujours en premier !!! (variable de configuration)
include "res/ip_somnolence.php"; // outil de resiliance
include "res/_divers_.php"; // fonctions inclasable mais utilisé partout...
include "res/res_linguistique.php"; /* pour rendre tout ça multilangue... */
$corps= include "res/directiveomega.php"; // en cas de toute premiere utilisation
include "res/res_cal.php"; //resources specialement pour manipuler les fichier ICS
include "res/res_cache.php"; // ça... ben c'est pour les fichiers de cache.




//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// recupère tout le $_GET asemblé en un seul text pour etre remis dans les lien de retour des les interface d'admin //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$url_tab=parse_url($_SERVER['REQUEST_URI']);
$query = isset($url_tab['query']) ? '?' . $url_tab['query'] : '';
if(!isset($url_tab['query']))$url_tab['query']="";	/* peu etre inutile */


///////////////////////////////////////////
// gestion de connexion et de session... //
///////////////////////////////////////////
$session=include("res/session_interface.php");


////////////////////////////////////////////////////
// determination de l'affichage demandé via l'URL //
////////////////////////////////////////////////////
$tab_req=determination_date_maitresse();//tableau reprenant le parsage de $_GET['aff'] si existant, sinon de $affichage_par_defaut
$metacal=determination_metacalendrier();//nom du metacalendrier demandé via $_GET['cal']... (pas encore fonctionel)


/////////////////////////////////////////////////////
// Gestion des truc qu'on peu faire en étant logué //
/////////////////////////////////////////////////////
$admin=false; //initialisation...
//$traiter_cal=true; //afficher le calendrier ?
if($traiter_cal){
	if(isset($_SESSION['log'])){ //si une session est ouverte
		if(!isset($nb_cal))metalistage(); /* sans doute à alléger !!! */

		/////////////////////////////////////////
		// invocation de l'editeur d'evenement //
		/////////////////////////////////////////
		include "res/monkey_date_invocation.php";


		if(isset($_POST["admin"])){ //Interface d'administration...
			/* ranger ça ailleur ? */
			$admin=true;
			$traiter_cal=false;
			if($_SESSION['niv']<=3 && isset($_POST["admin_cal"])){ //admin calendriers
				$corps=include("res/admin_calendrier.php");
			}elseif (isset($_POST["admin_user"])){
				if ($_SESSION['niv']<=2 || $_POST["admin_user"]=="#perso"){ //admin utilisateurs
					$corps=include("res/admin_utilisateur.php");
				}else{
					echo "tentativ d'usrurp... ";/* faire quelque chose de très méchant */
				}
			}elseif($_SESSION['niv']<=3){	//admin général
				$corps=include("res/admin.php");
			}elseif($_SESSION!=0 // le super_admin ne doit JAMAIS disparaitre !!!
					&& $_POST["admin"]=="suppr_user"
					&& $_POST["user"]=="#perso"
					&& isset($_POST["unlock_supr_1"]) && isset($_POST["unlock_supr_2"])){
				// supression du compte personel
				supprime_user($_SESSION["log"]);
				$admin=false; //on remet le calendrier normal...
				$traiter_cal=true;
				session_destroy();// Deconecssion
				$session=include("res/session_interface.php");

			}else{
				echo "tentative d'usurpation de niveau !!! ";/* faire quelque chose de très méchant */
			}
		}
	}
}






if($traiter_cal){ //si on doit afficher un calendrier...

	//////////////////////////////////////////////////////
	// traitement calendrier, cache, html, affichage... //
	//////////////////////////////////////////////////////

	if($developement)echo "<br>[T01:".(microtime(true)-$temps_de_traitement)."]";$temps_de_traitement=microtime(true);
	if(!isset($nb_cal))metalistage(); /* sans doute à alléger !!! */
	/*separer l'ajout de bouton d'edition avec une balise invisible à replacerar exemple... */
	$corps=include "res/traitement_cal.php"; /*** cest le morceau le plus long (0.3 à 0.7 secondes juste avec les calendrier locaux... plusieur seconde parfois avec les distant) */
	$corps.=include "res/res_menu_bas.php";
	if($developement)echo "<br>[T02:".(microtime(true)-$temps_de_traitement)."]";$temps_de_traitement=microtime(true);

}


//////////////////////////////////////
// entre les balises "head" html... //
//////////////////////////////////////
$head=include("res/head.php");



//////////////////////////////////
// barre du haut de la page web //
//////////////////////////////////
$entete=include("res/res_menu_haut.php");


/************************************/
/* generation finale de la page web */
/************************************/
echo $head;
echo $entete;
echo $corps;


if ($developement){

	//Toutes les variables qui trainent...
	$global_rest=get_defined_vars();

	//Moins les valriable de configuration (voir "_config_.php")
	foreach($config_global as $i => $j)if(isset($config_global[$i]))unset($global_rest[$i]);
	unset($global_rest["config_global"]);


	foreach($config_global as $i => $j)echo "ttt[$i=$j]<br />";

	//moins les trucs critiques hors code ($_POST $_GET ...)
	foreach($global_rest as $i => $j)if($i[0]=="_")unset($global_rest[$i]);
	//moins les truc de dev qui apparaissent dans "index.php"
	unset($global_rest['developement']);
	unset($global_rest['temps_de_traitement']);
	//moins les trucs qu'il ne faut pas toucher...
	unset($global_rest['MeshCal_vertion']);
	//moins les repertoires !!!
	foreach($global_rest as $i => $j)if(substr($i,0,4)=="rep_")unset($global_rest[$i]);
	//moins les machins qui ne servent pas encore à quelquechose
	unset($global_rest['theme']);
	unset($global_rest['langue']);
	unset($global_rest['metacal']);

	//on verra après...
	unset($global_rest['metadonees_calendriers']);

	//ce qui est affiché
	unset($global_rest['corps']);


	echo "<hr /><hr />Variables qui trainent...<pre>";
	print_r($global_rest);
	echo "</pre>";

	echo "<hr />\$_SESSION<pre>";
	print_r($_SESSION);
	echo "</pre><hr />\$_POST<pre>";
	print_r($_POST);
	echo "</pre><hr />\$metadonees_calendriers<pre>";
	print_r ($metadonees_calendriers);
	echo "</pre>";
}



?>
<hr />
<a href="https://www.gnu.org/licenses/agpl.html"><img src="https://www.gnu.org/graphics/agplv3-88x31.png" alt="[Small GNU AGPLv3 logo]"></a>
<a href="CeCILL 2.1">[CeCILL 2.1]</a>
<!-- trouvé ici : http://creativecommons.org/choose/ -->
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png" /></a> <span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/InteractiveResource" property="dct:title" rel="dct:type">MeshCal</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://meshcal.net" property="cc:attributionName" rel="cc:attributionURL">Jérôme Leignadier-Paradon</a>.
</body><html>
