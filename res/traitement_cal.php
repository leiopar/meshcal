<?php
// https://meshcal.net

// CC BY SA - Jérôme Leignadier-Paradon (2015)
// Creative Commons 4.0
// http://creativecommons.org/licenses/by-sa/4.0/


/*	Afero GPL v3
	MeshCal - Calendar server (whis rfc 2445 and 5545)
    Copyright (C) 2015 Jérôme Leignadier-Paradon

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see :
	https://www.gnu.org/licenses/agpl.html
*/

// CeCILL 2.1 - Jérôme Leignadier-Paradon (2015)
// http://www.cecill.info/faq.fr.html

//------------------------------------------------------------------------------


//gestion des calendriers...
// création des affichage, gestion des cache des erreurs...
/* prevoir un parametre $_POST pour forcer le recontrol des ICS distant me si la durée n'est pas depassée */

/*** LES MESSAGES D'ERREUR EN CACHE !!! */


include "res/res_ics.php"; //resources specialement pour traiter les fichier ICS
include "res/res_html.php"; //resources specialement pour creer le code HTML


//--------------------------------------------------------------------------------------


//avant tout : existe t'il le fichier cache necesaire ?
$nom_cache=nom_cache($tab_req).".html"; //nom du fichier cache nécésaire...

//	(A)

if(!$cache){ // si la fonction de cache est bloqué par defaut
	$cache_ok=false;
	$cal_distant_ok=false;
	$cal_distant_trop_vieu=true; /*** creer une option pour bloqué les sauvegardes locales des ICS distants */
	$cal_distant_plus_de_30s=true;
}else{ // sinon... c'est qu'on doit etre malin :-)
	$cache_ok=(file_exists($rep_cache.$nom_cache) && file_exists($rep_cache."touch"));//teste de l'existance du fichier concerné
	$cal_distant_ok=true; // passera à "false" si modif d'un ICS distant et declanchera un BOOMcache !
	if (!file_exists($rep_cache."touch")){
		$cal_distant_trop_vieu=true;
	}else{
		$cal_distant_trop_vieu=((time()-filemtime($rep_cache."touch"))>(3600*$ics_distant_delta_control));
		$cal_distant_plus_de_30s=((time()-filemtime($rep_cache."touch"))>30);
	}
}


// si demande de forcage de retéléchargement de calendrier distant 
// par sécurité, tout ne peu etre recharger que si la dernière verif date de plus de 30 secondes !!! (histoir de ne pas couller le serveur)
if(isset($_POST["recontrol_cal_distant"]))if($cal_distant_plus_de_30s){
	$cal_distant_trop_vieu=true;
}else{
	$moa=max(2, 30-(time()-filemtime($rep_cache."touch")));
	$erreurs[]=array("message" => "Antendez encore ".$moa." secondes avant de pouvoir recharger les calendriers distant.","en_cache" => false);
	$affichage_erreur=true;
}



if($developement)echo "<br>[C041:".(microtime(true)-$temps_de_traitement)."]";$temps_de_traitement=microtime(true);


//début de traitement ics





///////////////////////////////////////////////////////////////////////////
// si le dernier contro des ICS distant est trop vieux on recontrol tout //
///////////////////////////////////////////////////////////////////////////

if(!$cache_ok || $cal_distant_trop_vieu){ //s'il n'y a pas de fichier cache ou que les ICS distants datent trop...
	// première passe determination si les calendrier distant ont changer et chargement des calendriers distants
	foreach($metadonees_calendriers as $id_cal => $cal)if ($cal["type"]!="local"){ // si calendrier d'un autre site...


		/*tester l'existance d'une sauvegarde local !!! */
		$fichier_local=$rep_cal_dist_sauv.$cal["nom_local"].".ics"; //fichier de sauvegarde local
		if ($cal_distant_trop_vieu || !file_exists($fichier_local)){
			// chargement du fichier distant
			$file=@file_get_contents($cal["adresse"]); /** peu etre faire un netoyage de sécurité ? */
			if($file===false){// si le fichier est indispo
		
				//fichier absent, certe, mais ne remet pas le cache en cause
				if(file_exists($fichier_local) && file_exists($fichier_local.".hash")){
					$erreurs[]=array("type"=>"fichier distant introuvable.","id_cal"=>$id_cal, "url_cal"=>$cal["adresse"],"en_cache"=>true);
				}else{
					$erreurs[]=array("type"=>"fichier distant et sauvegarde locale introuvable.","id_cal"=>$id_cal, "url_cal"=>$cal["adresse"],"en_cache"=>true);
				}

			}else{ //le fichier est bien chargé
				if(file_exists($fichier_local) && file_exists($fichier_local.".hash")){
					/* tester s'il est inchanger */
					$metadonees_calendriers[$id_cal]["file"]=$file; /*** peu etre faire un netoyage de sécurité ? */
					
					$hash["distant"] = hash('md4', $file); // calcul de somme de control du fichier distant...
					$hash["local"] = file_get_contents($fichier_local.".hash");
					if($hash["distant"] == $hash["local"]){
						$moa=true;
					}else{
						$cache_ok=false;
						$moa=false;
					}
				}else{
					// sans sauvegarde local, dans le doute, on recharge...
					$cache_ok=false;
					$moa=false;
				}
				if(!$moa){
					// regénération de la sauvegarde local...
					nouvelle_sauvegarde_ics($metadonees_calendriers[$id_cal]);
				}
			}

		}		
	}
	// on acte la date du dernier control des fichier distants
	touch($rep_cache."touch");
}



if($developement)echo "<br>[C123:".(microtime(true)-$temps_de_traitement)."]";$temps_de_traitement=microtime(true);





////////////////////////////////////////////////////////////////////////
// deuxieme passe si nécéssaire pour finir de charger les calendriers //
////////////////////////////////////////////////////////////////////////
if(!$cache_ok)foreach($metadonees_calendriers as $id_cal => $cal){
	if ($cal["type"]=="local"){
		$metadonees_calendriers[$id_cal]["file"]=file_get_contents($rep_cal.$cal["adresse"]); //(télé)chargement des calendrier locaux.
	}else{ // sinon c'est que se sont les distants...
		if($cal["file"]=="" && file_exists($rep_cal_dist_sauv.$cal["nom_local"].".ics"))$metadonees_calendriers[$id_cal]["file"]=file_get_contents($rep_cal_dist_sauv.$cal["nom_local"].".ics");	
	}
}



	//echo "<br>[C149:".(microtime(true)-$temps_de_traitement)."]";$temps_de_traitement=microtime(true);



///////////////////////
// traitement ics... //
///////////////////////
if(!$cache_ok){//il faut régénéré les caches... donc on retraite les ics.
	if($cal_distant_ok==false)BOOMcache(); //s'il y à un problème, c'est que tout les caches sont moisi...
	$cal_aff=determination_plage_de_dates($tab_req);//Determination des plage de date à couvrir

	foreach($metadonees_calendriers as $id_cal => $cal)// pour tous les calendriers... convertion en tableau.
		IcsTab2Tabs($cal["file"],$id_cal,$cal_aff["debut_horizon"],$cal_aff["fin_horizon"]+(60*60*24)); /*** pourquoi "60*60*24" ??? sinon ca oubli le dernier jour !!! */ //parse les chaines ics en alimentant les deux tableaux principaux...
	
	ksort($liste_evens_occurences); //trie par jours	
	foreach($liste_evens_occurences as $j => $h)ksort($liste_evens_occurences[$j]); //trie par heures
	
	switch ($tab_req[0]) {
		case "A": //Année
			$ret='<link rel="stylesheet" type="text/css" href="'.$rep_themes.$theme.'annee.css" media="all">'; /*a rendre configurable*/ //styles...
			$ret.=tabs2html_annee($cal_aff); //affichage html sur un an.
			break;
		case "M": //mois
			$ret='<link rel="stylesheet" type="text/css" href="'.$rep_themes.$theme.'mois.css" media="all">'; /*a rendre configurable*/ //styles... 
			$ret.=tabs2html_mois($cal_aff); /* affichage en jours */
			break;
		case "S": //semaine
			$ret='<link rel="stylesheet" type="text/css" href="'.$rep_themes.$theme.'jours.css" media="all">'; /*a rendre configurable*/ //styles...
			$ret.=tabs2html_jours($cal_aff); /* affichage en jours */		
			break;
		case "J": //Journée
			$ret='<link rel="stylesheet" type="text/css" href="'.$rep_themes.$theme.'jours.css" media="all">'; /*a rendre configurable*/ //styles...
			$ret.=tabs2html_jours($cal_aff); /* affichage en jours */
			break;
		case "L": //liste
			/*** tout et à faire... ou pas pour pouvoir peronaliser facilement l'insertion de la liste dans un site diférant*/
			$ret='<link rel="stylesheet" type="text/css" href="'.$rep_themes.$theme.'liste.css" media="all">'; /*a rendre configurable*/ //styles...
			$ret.=tabs2html_liste($cal_aff);
			break;
	}

	//////////////////////////////////
	// création du fichier cache... //
	//////////////////////////////////
	if($cache){
		file_put_contents ($rep_cache.$nom_cache, $ret);
		touch($rep_cache."touch");
		/*** faire le fichier d'erreur de cache ... du coup on peu s'en servire à la place de "touch" ? */
		$moa="";

		foreach ($erreurs as $er)if($er["en_cache"]==true){
			if($moa != "")$moa.="\r\n";			
			foreach($er as $i => $j)$moa.=$i.'`'.$j."|";
		}
		file_put_contents ($rep_cache."erreurs.csv", $moa); /*** c'est pas tout de le faire, il va faloir l'exploiter !!! */
		cache_limit_control();// supression des fichier de cache en trop celon configuration (nombre max et taille total)
	}
}



	//echo "<br>[C210:".(microtime(true)-$temps_de_traitement)."]";$temps_de_traitement=microtime(true);


if($cache_ok){
	/////////////////////////////////////////////////
	// sinon charger le fichier de cache dans $ret //
	/////////////////////////////////////////////////
	$ret=file_get_contents ($rep_cache.$nom_cache);
	touch($rep_cache.$nom_cache);
	
	// avec les erreur d'ICS enregistré dans ...cache/erreurs.csv
	$fichier = $rep_cache."erreurs.csv";
	if(!file_exists($fichier)){
		// echo "pas d'erreur !";
	}else{
		$csv = fopen($fichier, 'r');
			while (($i = fgetcsv($csv, 1000, "|")) !== FALSE) {
				$k=array();
				foreach($i as $j){
					$l=explode("`", $j);
					if($l[0]!="")$k[$l[0]]=$l[1];// il peu y avoir un dernier champ libre
				}
				$erreurs[]=$k;
			}
		fclose($csv);
	} 
}



	//echo "<br>[C240:".(microtime(true)-$temps_de_traitement)."]";$temps_de_traitement=microtime(true);


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// si connecté, ajouter les boutons "[+]" pour permetre la création(et modification) d'event avec monkeydate //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
if($nb_cal["locaux"]>0)if(isset($_SESSION['log']))if($_SESSION['niv']<6)if($_SESSION['niv']<=3 || isset($_SESSION['perm'])){ //*** si seulement il existe au moin un calendrier local, si conecté et non banni...
	
	// donc : droit d'ecriture dans au moins un calendrier...
	$form["debut"]='
		<form method="post" action="'.$query.'" class="appelle_monkeydate_creer">
			<input type="hidden" name="Monkey_Date" value="creation">
			<input type="hidden" name="origine" value="'.
				$query. /* peu etre inutile ? */
			'" />
			<input type="hidden" name="date" value="';
	$form["fin"]='" />
			<input type="submit" value="+" />
		</form>';

	$ret=str_replace("<!--[",$form["debut"],$ret);
	$ret=str_replace("]-->",$form["fin"],$ret);

	$droit_edition_event=min(3,$_SESSION['niv']);
	//0		Super admin
	//1		administrateur
	//2		Organisateur
	//3		editeur
	//4		ajout d'information dans un cal
	//(5)	pas de privilège particulier (état par defaut)
	//(6, 7)banni, tout privilège bloqué par defaut


	
	// pour tout les calendrier autorisé en modification
		/* faire aparaitre les boutons de modification */

	foreach($metadonees_calendriers as $id_cal => $cal){
		$modif=false;
		//if (isset($privilege[$id_cal]))if($privilege[$id_cal]<=4) $modif=true; // si minimum droit d'ajout d'info dans le calendrier...		
		//if ($droit_edition_event<=3 || $modif){ // si minimum droit de création d'event dans au moins un calendrier...
			$form["debut"]='
				<form method="post" action="'.$query.'" class="appelle_monkeydate_modifier">
					<input type="hidden" name="Monkey_Date" value="modification" />
					<input type="hidden" name="origine" value="'.
						$query. /* peu etre inutile ? */
					'" />'.
					'<input type="hidden" name="cal_event" value="'.$id_cal.'" />'.
					'<input type="hidden" name="id_event" value="';
			$form["fin"]='" />
					<input type="submit" value="m" />
				</form>';
			
			// <!--{id_cal}id_event{/id_cal}-->
			$ret=str_replace("<!--{".$id_cal."}".$id_cal."|",$form["debut"],$ret);
			$ret=str_replace("{/".$id_cal."}-->",$form["fin"],$ret);
		//}
	}
}


//////////////////////////////
// Génération de la page... //
//////////////////////////////
return $ret;

?>
