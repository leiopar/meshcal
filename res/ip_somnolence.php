<?php
//$temps_mini_norm=0.5; // c'est dans "_config_.php"
$ip=$_SERVER['REMOTE_ADDR'];
$andicap=0;
$fichier='ip_morfals.csv';

//chargement du fichier dans $fic
$csv_new=array();
if (file_exists($rep_bases.$fichier)) if ($csv = fopen($rep_bases.$fichier, 'r')){//fichier de base de données des utilisateur
	while (($tab = fgetcsv($csv, 1000, "|")) !== FALSE){
		// $tab[0] = adresse ip
		// $tab[1] = microtime de la dernière visite
		// $tab[2] = andicap
		// $tab[3] = traces pour eventuel log de suspects

		$depuis=microtime(true)-$tab[1];
		$tempo=$temps_mini_norm*pow(2, $tab[2]/2);//progression géométrique
		
		if($depuis>$tempo){
			$tab[2]=max(0, $tab[2]-2); // on se calme
		}else{
			$tab[2]+=1; // on s'énerve !!!
		}

		if ($tab[2]>0)$csv_new[$tab[0]]=implode("|",$tab)."\r\n"; //pour remetre la ligne dans le fichier

		if ($ip==$tab[0]){ // si la ligne concer l'ip cliente...
			$andicap=$tab[2];
			$doucement=ceil($tempo);//(ceil=arrondi sup) calcul du temps d'attent à imposer avec progréssion géométrique
		}
	}
	fclose($csv);
}

if(!isset($csv_new[$ip]))$csv_new[$ip]=$ip."|".microtime(true)."|0|trucs";//au besoin on cré le marqueur de visite

file_put_contents ( $rep_bases.$fichier, implode("\r\n",$csv_new));// remise du fichier à sa place

//punition !!!
if($andicap>0 && !$developement)sleep($doucement); // création d'un delait pour les ip trop gourmandes

//netoyage...
unset($temps_mini_norm);
unset($ip);
unset($andicap);
unset($fichier);
unset($csv_new);
unset($csv);


?>
