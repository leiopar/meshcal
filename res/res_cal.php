<?php
// https://meshcal.net

// CC BY SA - Jérôme Leignadier-Paradon (2015)
// Creative Commons 4.0
// http://creativecommons.org/licenses/by-sa/4.0/


/*	Afero GPL v3
	MeshCal - Calendar server (whis rfc 2445 and 5545)
    Copyright (C) 2015 Jérôme Leignadier-Paradon

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see :
	https://www.gnu.org/licenses/agpl.html
*/

// CeCILL 2.1 - Jérôme Leignadier-Paradon (2015)
// http://www.cecill.info/faq.fr.html

//------------------------------------------------------------------------------


// resource de manipulation des fichier ical et des dates



//creer les premier tableaux :
//		$tab["cal"] ressance les calendrier et leur "métadonnées" générales
//		$tab["privileges"] decrit les privilège de l'utilisateur courant calendrier par calendrier
//
//				0, 1	admin
//				2		création d'event dans un cal
//				3		modification d'event dans un cal
//				4		seulement ajout d'information dans un cal
//				(5)		(depart) pas de privilège particulier (état par defaut)
//				(6, 7)	(depart) banni, tout privilège bloqué même ceux defini par les config de calendrier
//
//		$tab["nb"]["totaux"] donne le nombre de calendrier locaux
//		$tab["nb"]["locaux"] donne le nombre de calendrier locaux
//		$tab["nb"]["distant"] donne le nombre de calendrier locaux


//Determine l'orizon autorisé en années
$annee_en_cour=strftime("%Y")+0; //année courante
$global_horison_min=$annee_en_cour-$global_horison_avant;
$global_horison_max=$annee_en_cour+$global_horison_apres;


////////////////////////////////////////////////////
// determination de l'affichage demandé via l'URL //
////////////////////////////////////////////////////
function determination_date_maitresse(){
	global
		$affichage_par_defaut,
		$liste_horison_apres, //horison aprés par defaut
		$liste_horison_avant; //horison avant par defaut

	$tab_req=array();//tableau reprenant le parsage de $_GET['aff'] si existant, sinon de $affichage_par_defaut	
	
	if(isset($_GET['aff'])){ //si un parametre d'affichage est passé dans l'URL
		$tab_req=explode("-",$_GET['aff']);
	}else{ //sinon reprendre l'affichage par defaut
		$tab_req=explode("-",$affichage_par_defaut);
	}
	/*TODO utiliser "$global_horison_avant" et "$global_horison_apres" pour limiter */
	


	//replisage des info manquates avec les valeurs du jour courant
	if($tab_req[0][0]=="S"){//si affichage ebdomadaire, identifier le numero de semaine courant et le numero de jour dans la semaine

		if(!isset($tab_req[1]))	$tab_req[1]="";
		$tab_req[1]=annee_control($tab_req[1]);

		if(!isset($tab_req[2]))	$tab_req[2]=strftime("%V")+0; //semaine courante
		if($tab_req[2]=="")		$tab_req[2]=strftime("%V")+0;
		if(!isset($tab_req[3]))	$tab_req[3]=strftime("%u")+0; //jour courant de la semaine
		if($tab_req[3]=="")		$tab_req[3]=strftime("%u")+0;
	}elseif($tab_req[0][0]=="L"){
			/*** risque de poser un problème... */
		if(!isset($tab_req[1]))	$tab_req[1]=$liste_horison_apres; //horison aprés par defaut
		if($tab_req[1]=="")		$tab_req[1]=$liste_horison_apres;
		if(!isset($tab_req[2]))	$tab_req[2]=$liste_horison_avant; //horison avant par defaut
		if($tab_req[2]=="")		$tab_req[2]=$liste_horison_avant;

		if(!isset($tab_req[3]))	$tab_req[3]="";
		$tab_req[3]=annee_control($tab_req[3]);

		if(!isset($tab_req[4]))	$tab_req[4]=strftime("%m")+0; //mois courant
		if($tab_req[4]=="")		$tab_req[2]=strftime("%m")+0;
		if(!isset($tab_req[5]))	$tab_req[5]=strftime("%d")+0; //jour courant du mois
		if($tab_req[5]=="")		$tab_req[3]=strftime("%d")+0;
	}else{//sinon on prends la date classique

		if(!isset($tab_req[1]))	$tab_req[1]="";
		$tab_req[1]=annee_control($tab_req[1]);

		if(!isset($tab_req[2]))	$tab_req[2]=strftime("%m")+0; //mois courant
		if($tab_req[2]=="")		$tab_req[2]=strftime("%m")+0;
		if(!isset($tab_req[3]))	$tab_req[3]=strftime("%d")+0; //jour courant du mois
		if($tab_req[3]=="")		$tab_req[3]=strftime("%d")+0;
	}

	return $tab_req;
}



//limite l'année demandée à "$global_horison_avant" et "$global_horison_apres"
function annee_control($an=""){
	global $annee_en_cour, $global_horison_min, $global_horison_max;
	
	if($an==""){
		$ret=$annee_en_cour;
	}else{
		$ret=$an;
		if($an<$global_horison_min || $an<1971){ //le timestamp commencant le 01/01/1970...
			$ret=$global_horison_min;
			/*TODO message erreur */
		}
		if($an>$global_horison_max){
			$ret=$global_horison_max;
			/*TODO message erreur */
		}
	}
	return $ret;
}


/* pas encore utilisé (pas controlé) */
function determination_metacalendrier(){
	global $rep_bases, $erreurs, $metacal;
	$ret=$metacal; //metacalendrier par defaut

	// si un metacalendrier spécifique est demandé...
	if(isset($_GET['cal']))if(file_exists($rep_bases."metacal/".$_GET['cal'].".csv")){
		$metacal=$_GET['cal'];
	}else{
		$erreurs[]=array("type"=>"metacal_introuvable","metacal"=>$_GET['cal'] ,"en_cache"=>false);
	}

	return $ret;

}


////////////////////////////////////////////
// renseignement des $metadonees générale //
////////////////////////////////////////////
function metalistage(){
	global $metadonees_calendriers, $privilege, $nb_cal, $traiter_cat;
	$tab=listage_metadonees_calendriers(); /*** cette operation est lourde, voir si elle est bien nécésaire en cas d'utilisation d'un fichier cache et si l'utilisateur n'est pas conecté */
	/*echo "<hr><pre>";
	print_r($tab);
	echo "</pre><br>";*/
	$metadonees_calendriers=$tab["cal"]; //tout les calendrier...
	$privilege=$tab["privileges"]; //les autorisation spécific à l'utilisateur calendrier par calendrier...
	$nb_cal=$tab["nb"]; //nombre de calendrier (totaux, distant, local)
	$traiter_cat=true; //afficher le calendrier ?

}


function listage_metadonees_calendriers(){
	global $rep_bases, $metacal;
	$tab["cal"]=array();
	$tab["privileges"]=array();
	$tab["nb"]["totaux"]=0;
	$tab["nb"]["locaux"]=0;
	$tab["nb"]["distant"]=0;



/* trantement de metacal */
/* avec des bout de ça : ($cal = fopen($rep_bases."metacal/".$metacal.".csv", 'rb')) */
	if ($cal = fopen($rep_bases."calendriers.csv", 'rb')){ //liste de tout les calendrier du metacalendrier demandé à afiché avec leur métadonnées
		while (($i = fgetcsv($cal, 1000, "|")) !== FALSE){ // parcour...
			$id_cal=trim($i[0]);
			if (substr($id_cal,0,2)!="//" && substr($id_cal,0,1)!="#" && $id_cal!=""){// si ce n'est pas une ligne de comentaire ou une ligne vide alors on renseigne les metadonnées...
				$tab["cal"][$id_cal]["nom"]=trim($i[1]);
				$tab["cal"][$id_cal]["adresse"]=trim($i[2]);
				$tab["cal"][$id_cal]["type"]=trim($i[3]);
				$tab["nb"]["totaux"]++;
				if($tab["cal"][$id_cal]["type"]!="local"){ //si canendrier distant
					$tab["nb"]["distant"]++;
					$tab["cal"][$id_cal]["nom_local"]=preg_replace('@[^a-zA-Z0-9_]@', '_', $tab["cal"][$id_cal]["adresse"]); /*** nom de fichier localisé netoyer à traiter et sauvegarder ailleur... */
				}else{ //sinon si calendrier local on renseigne les privilège d'édition acordé à l'utilisateur courant
					$tab["nb"]["locaux"]++;
					if(isset($_SESSION['log']))if($_SESSION['niv']<=5){//si logué et pas banni on liste les autorisations
						$liste=substr($tab["cal"][$id_cal]["adresse"], 0, -4).".csv"; //(nom du fichier contenant la liste des utilisateurs du calendrier)
						if (file_exists($rep_bases.'cal/'.$liste)){
							$users = fopen($rep_bases.'cal/'.$liste, 'rb');
						
						//if (!($users===false))
							
							while (($j = fgetcsv($users, 1000, "|")) !== FALSE){// parcour de la liste
								$id_user=trim($j[0]);
								if (substr($id_user,0,2)!="//" && substr($id_user,0,1)!="#" && $id_user!=""){ // si ce n'est pas un comentaire...
									if ($id_user=="@all" || $id_user==$_SESSION['log']){ /*atention à interdir la création d'un utilisateur dont l'identifiant commence par "@" */ //le meta utilisateur "@all" designe des droits donnés à tous par defaut
										$niv=$tab["privileges"][$id_cal]=((isset($j[1])?trim($j[1]):"")==""?2:trim($j[1]));
										$tab["privileges"][$id_cal]=(isset($tab["privileges"][$id_cal])?min($tab["privileges"][$id_cal],$niv):$niv); //au choix on prend le plus permissif
									}
								}
							}
							fclose($users);
						}
						
					}
				}
				$tab["cal"][$id_cal]["file"]=""; //création de la place pour le contenu des fichier
			}
		}
		fclose($cal);
	}
	
	/*echo "<hr>metadonees_calendriers<pre>";
	print_r($tab);
	echo "</pre><hr>";*/
	
	return $tab;
}




////////////////////////////////////////////////////////////////
// netoyage de contenu textuel venant de calendrier divers... //
////////////////////////////////////////////////////////////////
function netoyage_divers($text,$type="html"){
	$text=str_replace('\,',',',$text);
	$text=str_replace('\;',';',$text);
	$text=str_replace("&amp;","&",$text);
	if ($type=="html"){
		$text=str_replace('&#13;','<br>',$text);
	}else{
		$text=str_replace('&#13;'."\n","\n",$text);
		$text=str_replace('&#13;',"\n",$text);
	}
	return $text;
}




//////////////////////////////////////////////////////////////////
// determine les droits de l'utilisateur sur le calendrier $cal //
//////////////////////////////////////////////////////////////////
function droit_cal($cal){
	global $privilege, $rep_bases, $metadonees_calendriers;

	$lvl=7; //niveau par defaut (7 = bannissement ultime)	
	if($metadonees_calendriers[$cal]["type"]=="local"){	// controlé si calendrier bien local
		if (isset($_SESSION['niv'])){	 //si l'utilisateur est connecté
			if ($csv = fopen($rep_bases.'utilisateurs.csv', 'rb')){	// control si pas banni entretemps !!!
				while (($i = fgetcsv($csv, 1000, "|")) !== FALSE)
					if ($_SESSION['log'] == $i[0]) $_SESSION['niv']=$i[3];
				fclose($csv);
			}else{
				/*sinon c'est que la base est inaxessible */
			}
			$lvl=$_SESSION['niv'];
			if ($_SESSION['niv']<6)	//si pas banni
				if(isset($_SESSION['perm'][$cal]))$lvl=min($lvl, $_SESSION['perm'][$cal]);
		}else{
			/* tentative de hack html malveillant ... à punir ! */
		}
	}else{
		/* tentative de hack html malveillant ... à punir ! */
	}
	return $lvl;	
}








///////////////////////////////////////////////////////////////////////////////////////////////
// convertion d'un evenement venu de Monkey_Date dans $_POST en un bloc texte de type "ical" //
///////////////////////////////////////////////////////////////////////////////////////////////
function POST2event_ical(){
	/* pour traiter les erreures... */
	global $event_rappel;

	$even="BEGIN:VEVENT\n";
	$even.="CREATED:".date("Ymd\THis")."Z\n";
	$even.="UID:".date("YmdHis-").rand( 1000000 , 9999999 )."@".$_SERVER['HTTP_HOST']."\n"; 
	$even.="X-auteur:".$_SESSION['log']."\n";
	//$even.="X-validateur:"."jerome(ppra)"."\n"; /***/
	$titre=str_replace(":",'&#58;',$_POST['titre']); // remplacement des caractaires ":"
	$even.="SUMMARY:".htmlspecialchars($titre!=""?$titre:"(Nouvel évènement)")."\n";  /* couper en ligne de 74 caractaires + un " " au debut */ /** erreur si vide */
	/*echo "<hr><pre>";print_r($_POST); echo "</pre>";*/
	if($_POST['repet']!="jamais"){
		
		if($_POST['repet']=="jour")$even.="RRULE:FREQ=DAILY;INTERVAL=".$_POST['chaque']; //repetition tout les n jours
		if($_POST['repet']=="semaine")$even.="RRULE:FREQ=WEEKLY;INTERVAL=".$_POST['chaque'].";BYDAY=".implode(',', $_POST['jours']); /*** traitement si $_POST['jours'] vide !!! */ //repetition tout les n semaine
		if($_POST['repet']=="mois"){
			if($_POST['choix_repet']=="xeme")$even.="RRULE:FREQ=MONTHLY;INTERVAL=".$_POST['chaque'].";BYDAY=".$_POST['jour_a'].$_POST['jour_b']; //repetition tout le Xème jour tout les n mois
			if($_POST['choix_repet']=="num")$even.="RRULE:FREQ=MONTHLY;INTERVAL=".$_POST['chaque'].";BYMONTHDAY=".implode(',', $_POST['j']); //repetition tout les jour numero X tout les mois
		}
		if($_POST['repet']=="an"){
			//repetition tout le Xème jour tout les x mois tout les n ans comme : RRULE:FREQ=YEARLY;INTERVAL=2;BYMONTHDAY=15;BYMONTH=11
			if($_POST['choix_repet']=="xeme")$even.="RRULE:FREQ=YEARLY;INTERVAL=".$_POST['chaque'].";BYDAY=".$_POST['jour_a'].$_POST['jour_b'].";BYMONTH=".implode(',', $_POST['m']);
			//repetition tout les jour numero X tout les x mois tout les n ans
			if($_POST['choix_repet']=="num")$even.="RRULE:FREQ=YEARLY;INTERVAL=".$_POST['chaque'].";BYMONTHDAY=".implode(',', $_POST['j']).";BYMONTH=".implode(',', $_POST['m']);
		}
		if($_POST['repet_fin']=="nb")$even.=";COUNT=".$_POST['fin_nb_unit'];
		if($_POST['repet_fin']=="date"){
			$datef=DateTime::createFromFormat("d/m/Y", $_POST['date_fin']);
			$datef->add(new DateInterval('P1D'));
			$datef=$datef->format("Ymd");
			$even.=";UNTIL=".$datef."T000000Z";
		}
		$even.="\n";
	}
	
	$date1=DateTime::createFromFormat("d/m/Y", $_POST['date1']);
	$date1=$date1->format("Ymd");
	$date2=DateTime::createFromFormat("d/m/Y", $_POST['date2']);
	$date2=$date2->format("Ymd");
	if (!isSet($_POST['journee'])){
		$date1.="T".$_POST['heure1'].$_POST['minute1']."00";
		$date2.="T".$_POST['heure2'].$_POST['minute2']."00";
	}
	//$even.="DTSTART;TZID=Europe/Paris:".min($date1,$date2)."\n"; /*** timezone */
	//$even.="DTEND;TZID=Europe/Paris:".max($date1,$date2)."\n"; /*** timezone */
	$even.="DTSTART:".min($date1,$date2)."\n";
	$even.="DTEND:".max($date1,$date2)."\n";
	
	
	if($_POST['lieu']!="")$even.="LOCATION:".$_POST['lieu']."\n"; /* openstreetmap ? */
	if($_POST['url']!="")$even.="URL:".$_POST['url']."\n";	
	
	$description=htmlspecialchars($_POST['description']); 
	$description=str_replace( array(chr(13).chr(10) , chr(13) , chr(10)) , '&#13;' , $description);
	$description=str_replace(":",'&#58;',$description);
	
	if($_POST['description']!="")$even.="DESCRIPTION:".$description."\n"; /*** couper en ligne de 74 caractaires + un " " au debut */
	if (isSet($_POST['journee']))$even.="TRANSP:TRANSPARENT"."\n";
	
	$tab=explode('|',$event_rappel);//liste des alarmes avant evenement...
	foreach($tab as $i)$even.="BEGIN:VALARM\nACTION:DISPLAY\nTRIGGER;VALUE=DURATION:".$i."\nEND:VALARM\n"; /* a rendre plus intéligent ? */
	$even.="END:VEVENT\n";
	

	//echo "<hr><pre>";	/***/
	//print_r($even);		/***/
	//echo "</pre><hr>";	/***/

	/*decouper les ligne qui font plus de 80 caractaires !!! */
	


	$even=tasse_event_75($even);
	
	return $even;
}


////////////////////////////////////////////////////////////////////////////////////
// decoupe un event en troncon de 75 caractairs (conformément à la RFC 2445 §4.1) //
////////////////////////////////////////////////////////////////////////////////////
function tasse_event_75($even){ // TODO à remplacer par verssion débugué !!!
		
	$tab=explode("\n",$even);

	$tab2=array();
	foreach($tab as $i){
		if(strlen($i)<=80){
			$tab2[]=trim($i);

		}else{ //là c'est que c'est long
			$tab2[]=substr($i,0, 75);
			$k=str_split(substr(trim($i), 75), 74);
			foreach($k as $l)$tab2[]=" ".$l;
		}
	}

	$ret= implode("\n",$tab2);
	file_put_contents ( "even_ics.txt", $ret);
	return $ret;
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// insertion d'un nouvel evenement formaté en un bloc texte de type "ical"($even) dans un fichier calendrier ($fic_cal) //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function insert_event($even,$fic_cal){
	$moa = "";
	if ($fic=fopen($fic_cal, "r")) {
		while (($buffer = fgets($fic, 4096)) !== false) {
			if ( substr($buffer, 0 , 13) =="END:VCALENDAR")$moa.=$even;
			$moa.= $buffer;
		}
		if (!feof($fic)) {
			echo "Erreur: fgets() a échoué\n";
		}
		fclose($fic);
		file_put_contents($fic_cal, $moa);
	}else{
		echo "erreur de fichier dans insert_event !<br>"; /*** creer un message d'erreur... */
	}
}









///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Remplace (ou efface si "$nouveau" est vide au lieu de contenir l'evenement remplacent) l'evenement dont l'id est "$uid" dans le fichier calendrier "$cal" //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function remplace_event($cal, $uid, $nouveau=""){
//echo "<hr>".$cal; /***/
	$handle = fopen($cal, "r"); //ouverture du fichier calendrier
	if ($handle) {
		$levent="";$buf="";$bevent=false;
		while (($lcal = fgets($handle, 4096)) !== false) { //parcour ligne par ligne...

			if (substr($lcal, 0, 12) =="BEGIN:VEVENT")$bevent=true; //si debut d'un event
			if (substr($lcal, 0, 4) =="UID:")$id=trim(substr($lcal, 4, -1)); //récupération de l'id de l'event
			
			if ($bevent){ //si on est dans un event, on enregistre en concatenant dans la variable event
				$levent.=$lcal;
			}else{ // sinon c'est que se sont des ligne concernant l'ensamble du calendrier, on met dans le bufer
				$buf.=$lcal;
			}
			
			if ( substr($lcal, 0 , 10) =="END:VEVENT"){ //si c'est la fin de l'event 
				if ($id!=$uid){//et que ce n'est pas l'event désiré on buferise l'event,
					$buf.=$levent;
				}else{ //sinon c'est que c'est celui dont on ne veux plus donc on le remplace
					$buf.=$nouveau;
				}
				$levent="";$bevent=false; //et on se prépar pour les event suivant
			}
			
		}
		if (!feof($handle)) { /* gestion d'erreur à gérer... */
			echo "Erreur: fgets() a échoué\n";
		}
		fclose($handle); //fermeture du calendrier
		//echo "<hr><pre>".$buf."</pre>"; /***/
		file_put_contents($cal, $buf);/*** à sécuriser !!! ***/ //ecrasement du calendrier avec le bufer... qui est sans l'event à suprimer
	}                                                            
}               



///////////////////////////////////////////////////////////////////////////////////////////////
// Extretrait le contenu d'un event préci d'un calendrier donné au format bloc de texte ical //
///////////////////////////////////////////////////////////////////////////////////////////////
function capture_event($calid, $uid){
	global $metadonees_calendriers, $rep_cal_dist_sauv;
//echo "<hr>".$cal; /***/
	$handle = @fopen($metadonees_calendriers[$calid]["adresse"], "r"); /*ouverture de la sauvegarde local si distant indisponible ! *///ouverture du fichier calendrier distant
	if (!$handle) 	$handle = fopen($rep_cal_dist_sauv.$metadonees_calendriers[$calid]["nom_local"].".ics", "r");

	$levent="";$buf="";$bevent=false;
	while (($lcal = fgets($handle, 4096)) !== false) { //parcour ligne par ligne...

		if (substr($lcal, 0, 12) =="BEGIN:VEVENT")$bevent=true; //si debut d'un event
		if (substr($lcal, 0, 4) =="UID:")$id=trim(substr($lcal, 4, -1)); //récupération de l'id de l'event
		
		if ($bevent && 									//si on est dans un event...
			(substr($lcal, 0, 12) !="BEGIN:VEVENT") && 	//mais ni sur la première ligne...
			(substr($lcal, 0 , 10) !="END:VEVENT")		//ni sur la dernière...
		)$levent.=$lcal; 								//alors on enregistre en concatenant dans la variable event
		
		if (substr($lcal, 0 , 10) =="END:VEVENT"){ //si c'est la fin de l'event 
			if ($id==$uid)$event=$levent; /* faire un breack ? */ //et que c'est l'event désiré on enregistre...
			$levent="";$bevent=false; //et/ou on se prépar pour les event suivant
		}
		
	}
	if (!feof($handle)) { /* gestion d'erreur à gérer... */
		echo "Erreur: fgets() a échoué\n";
	}
	fclose($handle); //fermeture du calendrier
	
	
	/*echo '<hr>l event : <pre>';
	print_r($event);
	echo "</pre>";*/
	
	return $event;
}  




// transforme un text d'event en un tableau de variable nommé dudit event
/* (à tester... ) */
function texte_event2tab($texte){
	$tab_lignes=explode("\n", trim($texte)); //on separ le texte en lignes
	/*echo '<hr>$tab_lignes : <pre>';
	print_r($tab_lignes);
	echo "</pre>";*/
	foreach($tab_lignes as $ligne){ // regroupement des variable etant sur plisieur lignes (comme les description par exemple)
		if(trim($ligne)!=""){
			if (substr($ligne, 0,1)!=" "){ //si ce n'est pas une suite du texte de la précédente c'est que c'est une nouvelle variable
			
				//$variable=explode(":",$ligne); /*** ne marche pas avec "URL" ( à cause des ":" de "http://..." */
				//$tab[trim($variable[0])]=trim($variable[1]);

				$pos = stripos($ligne, ":");				
				$variable=trim(substr($ligne,0,$pos));
				$tab[$variable]=trim(substr($ligne,$pos+1));
			}else{ //sinon c'est que c'est une suite à concaténner...
				$tab[$variable].=substr($ligne,1);
			}
		}
	}
	
	
	if (isset($tab['RRULE'])){ // parsage des repetions...
		$rrtab=explode(";",$tab['RRULE']);
		unset($tab['RRULE']);
		foreach($rrtab as $i){
			$j=explode("=",$i);
			$tab['RRULE'][$j[0]]=$j[1];
		}
		foreach($tab['RRULE'] as $r => $i)
			if(substr($r,0,2)=="BY")
				$tab['RRULE'][$r]=explode(",",$i);

	}
	
	
	//parsage des date de début et de fin de l'event
	foreach($tab as $label => $val){
		if (substr($label, 0,8)=="DTSTART;" || substr($label, 0,6)=="DTEND;"){
			$i=explode(";",$label);
			$tab[$i[0]]=$val;
			$j=explode("=",$i[1]);
			$tab[$j[0]]=$j[1];
			unset($tab[$label]);			
		}
	}
	
	
	/*echo '<hr>$tab : <pre>';
	print_r($tab);
	echo "</pre><hr>";*/
		
	return $tab;
}



/////////////////////////////////////////////////
//	Return Unix timestamp from ical date time format 
//
//	@param {string} $icalDate A Date in the format YYYYMMDD[T]HHMMSS[Z] or YYYYMMDD[T]HHMMSS
// $opt= null ou 
//	"j" pour ne donner que les jours...
//	"s" le nombre de secondes depuis le début du jour
/////////////////////////////////////////////////
function iCalDateToUnixTimestamp($icalDate,$opt=null){ 

	$icalDate = str_replace('T', '', $icalDate); 
	$icalDate = str_replace('Z', '', $icalDate); 

	$pattern  = '/([0-9]{4})';   // 1: YYYY
	$pattern .= '([0-9]{2})';    // 2: MM
	$pattern .= '([0-9]{2})';    // 3: DD
	$pattern .= '([0-9]{0,2})';  // 4: HH
	$pattern .= '([0-9]{0,2})';  // 5: MM
	$pattern .= '([0-9]{0,2})/'; // 6: SS
	preg_match($pattern, $icalDate, $date); 

	// Unix timestamp can't represent dates before 1970
	if ($date[1] <= 1970) {
		return false;
	} 
	/* Unix timestamps after 03:14:07 UTC 2038-01-19 might cause an overflow if 32 bit integers are used.*/
	if ($opt=="j"){
		$timestamp = mktime(0,  			// 4: HH 
							1,  			// 5: MM  (ajout de 100 secondes pour eviter
							40,  			// 6: SS  les confusion de jour autour de minuit)
							(int)$date[2],  // 2: MM 
							(int)$date[3],  // 3: DD 
							(int)$date[1]); // 1: YYYY 
	}elseif ($opt=="s"){
		$timestamp = ((int)$date[4])*60*60+ ((int)$date[5])*60 + ((int)$date[6]);
	}else{
		$timestamp = mktime((int)$date[4],  // 4: HH 
							(int)$date[5],  // 5: MM 
							(int)$date[6],  // 6: SS 
							(int)$date[2],  // 2: MM 
							(int)$date[3],  // 3: DD 
							(int)$date[1]); // 1: YYYY 
	}
	return  $timestamp;	
}

function determination_plage_de_dates($tab_req){//Determination des plage de dates à couvrir
	$ret=array();
	switch ($tab_req[0]) {
		case "A": //Année
			$ret["debut"]=mktime(0, 0, 0, 1, 1, $tab_req[1]);//calcul du premier jour de l'année.
			$ret["debut_horizon"]=mktime(0, 0, 0, 12, 1, $tab_req[1]-1);//calcul du debut du mois de décsembre de l'année précédente.
			$ret["fin"]=mktime(0, 0, 0, 12, 31, $tab_req[1]);//calcul du dernier jour de l'année.
			$ret["fin_horizon"]=mktime(0, 0, 0, 1, 31, $tab_req[1]+1);//calcul de la fin du mois de janvier de l'année d'aprés.		
		    break;
		case "M": //mois
			$ret["debut"]=mktime(0, 0, 0, $tab_req[2], 1, $tab_req[1]);//calcul du premier jour du moi
			$j_s=(date("w", $ret["debut"])==0?7:date("w", $ret["debut"])); /* à internationaliser... */ /*voir "strftime" avec "%u" (et non "%w") */ // (numero de jour dans la semaine lundi =1, dimanche =7
			$ret["debut_horizon"]= $ret["debut"]-($j_s-1)*24*60*60;//calcul du premier jour de la semaine du premie jour du mois
			$ret["fin"]=strtotime("-1 day", mktime(0, 0, 0, $tab_req[2]+1, 1, $tab_req[1]));//calcul du dernier jour du mois
			$j_s=(date("w", $ret["fin"])==0?7:date("w", $ret["fin"])); /* à internationaliser... */ /*voir "strftime" avec "%u" (et non "%w") */ // (numero de jour dans la semaine lundi =1, dimanche =7
			$ret["fin_horizon"]=$ret["fin"]+(7-$j_s)*24*60*60;//calcul du dernier jour de la semaine du dernier jour du mois
			break;
		case "S":
			$premier_jour_de_l_annee=mktime(0, 0, 0, 1, 1, $tab_req[1]); /***/
			$j_s=(date("w", $premier_jour_de_l_annee)==0?7:date("w", $premier_jour_de_l_annee));
			if ($j_s>4){//semaine 01 commence l'année dernière
				$lundi_s0=strtotime("+".(8-$j_s)." day", $premier_jour_de_l_annee);
			}else{//semaine 01 commence cette année
				$lundi_s0=strtotime("-".($j_s-1)." day", $premier_jour_de_l_annee);
			}
			$ret["debut"]=strtotime("+".(7*($tab_req[2]-1))." day", $lundi_s0);
			$ret["debut_horizon"]=strtotime("-1 day", $ret["debut"]);//La veille
			$ret["fin"]   = strtotime("+6 day", $ret["debut"]);// de dimanche
			$ret["fin_horizon"]=strtotime("+7 day", $ret["debut"]);//le lundi suivant
		    break;		
		case "J":
			$ret["debut"]=mktime(0, 0, 0, $tab_req[2], $tab_req[3], $tab_req[1]);//LE jour de l'année
			$ret["debut_horizon"]=strtotime("-1 day", $ret["debut"]);//La veille
			$ret["fin"]=$ret["debut"];//un seul jour, forcément...
			$ret["fin_horizon"]=strtotime("+1 day", $ret["debut"]);//le lendemain
		    break;
	
		case "L":
			echo "liste sur <i>N</i> jours"; /***/
			$ret["debut"]=mktime(0, 0, 0, $tab_req[4], $tab_req[5], $tab_req[3]);//LE jour de l'année
			$ret["debut_horizon"]=strtotime("-".$tab_req[3]." day", $ret["debut"]);//avant le jour...
			$ret["fin_horizon"]=strtotime("+".$tab_req[2]." day", $ret["debut"]);//après le jour...
			$ret["fin"]=$ret["fin_horizon"];
			break;		
		
	}

	// controler que ça fait bien le boulo ???
		/*
		echo '<hr>';
		echo '$ret["debut_horizon"]'.date('l d F Y',$ret["debut_horizon"])."<br>";
		echo '$ret["debut"]'.date('l d F Y',$ret["debut"])."<br>";
		echo '$ret["fin"]'.date('l d F Y',$ret["fin"])."<br>";
		echo '$ret["fin_horizon"]'.date('l d F Y',$ret["fin_horizon"])."<br>";
		echo '<hr>';
		*/

	return $ret;
}
?>
