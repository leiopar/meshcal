<?php
// https://meshcal.net

// CC BY SA - Jérôme Leignadier-Paradon (2015)
// Creative Commons 4.0
// http://creativecommons.org/licenses/by-sa/4.0/


/*	Afero GPL v3
	MeshCal - Calendar server (whis rfc 2445 and 5545)
    Copyright (C) 2015 Jérôme Leignadier-Paradon

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see :
	https://www.gnu.org/licenses/agpl.html
*/

// CeCILL 2.1 - Jérôme Leignadier-Paradon (2015)
// http://www.cecill.info/faq.fr.html

//------------------------------------------------------------------------------


///////////////////////////////
// Génération du menu du bas //
///////////////////////////////
global $metadonees_calendriers;

if (count($metadonees_calendriers)>0){
	$ret='<hr><b>Calendriers : </b>'."\n";
	foreach($metadonees_calendriers as $id => $meta){
		$ret.='<span class="'.$id.'" >';
		$ret.=$meta['nom'];
		$ret.='</span><span class="exp">';
		$ret.='(<a href="'.$meta["adresse"].'">ical</a>)';
		$ret.='</span> ; '."\n";
	}
}




////////////////////////////////////////////
// Réapropriation des calendrier distants //
////////////////////////////////////////////
// formulaire
$ret.=recharge_cal_distant_form();




return $ret;


?>
