<?php
// https://meshcal.net

// CC BY SA - J�r�me Leignadier-Paradon (2015)
// Creative Commons 4.0
// http://creativecommons.org/licenses/by-sa/4.0/


/*	Afero GPL v3
	MeshCal - Calendar server (whis rfc 2445 and 5545)
    Copyright (C) 2015 J�r�me Leignadier-Paradon

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see :
	https://www.gnu.org/licenses/agpl.html
*/

// CeCILL 2.1 - J�r�me Leignadier-Paradon (2015)
// http://www.cecill.info/faq.fr.html

//------------------------------------------------------------------------------


//gestion des conexions d'utilisateur 


/** faire en sorte que les niveau par defaut soit control� r�guli�rement m�me si la session perdure (bannisement) */

session_start();




//limitation de la durr�e de session en cas d'inutilisation
if (isset($_SESSION['log'])) if (time()-$_SESSION['derniere_visite']>$session_non_visite_temps_max*60){
	unset($_SESSION['log']);
}else{
	$_SESSION['derniere_visite']=time();
}
if (!isset($_SESSION['log']))session_destroy();

//si nouvelle session
if (isset($_POST['log'])&&($_POST['log']!="")&&file_exists($rep_bases.'utilisateurs.csv')){
	if (isset($_SESSION['log']))session_destroy();//supression d'une eventuel conexion pr�c�dente
	//test indentit�
	$log=false;	
	if ($csv = fopen($rep_bases.'utilisateurs.csv', 'rb')){//fichier de base de donn�es des utilisateur

		while (($i = fgetcsv($csv, 1000, "|")) !== FALSE){ // identifiant | mots de passe (sha256) | adresse courriel | niveau | langue |autres infos...
		//for ($ligne = fgetcsv($f, 1024); !feof($f); $ligne = fgetcsv($f, 1024)) {
			if ($_POST['log'] == $i[0] && hash("sha256",$_POST['pas']) == $i[1]){
				session_start();
				$log = true;
				$_SESSION['derniere_visite']=time();
				$_SESSION['log']=$i[0];	//identifiant
				$_SESSION['cou']=$i[2]; /*TODO adresse couriel de l'utilisateur... est-ce n�c�saire ici ? */

				if($i[3][0]=="6"){ // si bani temporaire
					$moa=explode("@",$i[3]);
					$_SESSION['niv']=6;
					if(time()>$moa[1]){ // si periode de banissement fini
						$_SESSION['niv']=5;

						// reconstituer la ligne de l'utilisateur !!!
						$moa=array("niv" => 5); reconstitu_utilisateur($i[0],$moa);

					}else{
						$_SESSION["d_ban"]=$moa[1];
					}
				}

				$_SESSION['niv']=$i[3][0];	//niveau d'otorisation par niveau

				if ($_SESSION['niv']<6)if($i[4]!=""){//liste des calendrier autoris� � modification
					$moa=explode(';',$i[4]);
					foreach($moa as $j)$_SESSION['perm']["cal_".$j]=$j;
				}
				$_SESSION['lng']=$i[5];			
				//$_SESSION['autres infos...']=$i[4];
			}
		}

		fclose($csv);
	}
	if (!$log) {
		$erreurs[]=array("message" => message(4),"en_cache" => false); //"Identifiant ou mot de passe incorecte !
		$affichage_erreur=true;
	}
}



if (isset($_POST['logout'])) {
	if (isset($_SESSION['log'])){
		unset($_SESSION['log']);
		//session_destroy();
		//$_SESSION = array();
		//setcookie ("PHPSESSID", "", time() - 3600);	
	}
}
?>
