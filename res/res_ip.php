<?php
// https://meshcal.net

// CC BY SA - J�r�me Leignadier-Paradon (2015)
// Creative Commons 4.0
// http://creativecommons.org/licenses/by-sa/4.0/


/*	Afero GPL v3
	MeshCal - Calendar server (whis rfc 2445 and 5545)
    Copyright (C) 2015 J�r�me Leignadier-Paradon

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see :
	https://www.gnu.org/licenses/agpl.html
*/

// CeCILL 2.1 - J�r�me Leignadier-Paradon (2015)
// http://www.cecill.info/faq.fr.html

//------------------------------------------------------------------------------


// outil de resilience au pannes divers

// l'url � traiter
$url="http://www.agendadulibre.org/ical.php?region=22";

$tab = parse_url($url); 
echo "scheme = " . $tab["scheme"] . "<BR>"; 
echo "host = " . $tab["host"] . "<BR>"; 
echo "path = " . $tab["path"] . "<BR>"; 
echo "query = " . $tab["query"] . "<BR>"; 
$ip= gethostbyname($tab["host"]); //l'ip...
echo "IP = " . $ip . "<BR>";
$url_ip=$tab["scheme"]."://".$ip."/".$tab["path"]."?".$tab["query"]; //remplacement du Host par l'ip corespondante (et sauvegard� au pr�alable)
echo "url/ip = " . $url_ip . "<HR>";
// du coup, l� il me semble que j'ai un r�sultat exploitable non ?


$ips= gethostbynamel($tab["host"]); //les ip s'il y en � plusieur dans le DNS...
print_r($ips);
foreach($ips as $i){// liste des URL/IP possible...
	$url_ip=$tab["scheme"]."://".$i."/".$tab["path"]."?".$tab["query"];
	echo "url/ip = " . $url_ip . "<BR>";
}


/*TODO retarder les ip... "gourmande" ! */

$ip=$_SERVER['REMOTE_ADDR'];

//lecture du fichier de tracage ip





















// a partir de l�, je ne comprends plus � quoi ca sert...
$header = array
(
    "Host: ".$tab["scheme"]."://".$tab["host"], // IMPORTANT
    "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.3",
    "Accept-Encoding: gzip,deflate,sdch",
    "Accept-Language: fr-FR,it;q=0.8,en-US;q=0.6,en;q=0.4",
    "Cache-Control: max-age=0",
    "Connection: keep-alive",
);
$ch = curl_init($url);
curl_setopt($ch, CURLOPT_URL, $url_ip);
curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
?>






<?php

echo "<hr><hr>Obtient l'adresse IP correspondant au nom d''h�te : <br>";
$resultat = gethostbyname("www.yahoo.fr");
echo "IP = $resultat<br/>";

echo "<hr/>Obtient le nom d''h�te  correspondant � l''adresse IP : <br>";
$host = gethostbyaddr("192.0.34.166");
echo "Host = $host";

echo "<hr/>Obtient la liste des adresses IP correspondant au nom d''h�te : <br>";
$resultat = gethostbynamel("yahoo.fr");
print_r($resultat);

echo "<hr/>Obtient le Mail Exchange(MX) associ� au nom d''h�te : <br>";
$resultat = getmxrr("yahoo.fr",$mxhosts);
print_r($resultat);

echo "<hr/>Obtient les Alias pour le nom d''h�te : <br>";
$resultat = dns_get_mx("yahoo.fr", $mxhosts, $weights);
print_r($resultat);

echo "<hr/>V�rifier s''il ya un enr�gistrement DNS associ� au nom d''h�te (checkdnsrr): <br>";
$resultat = checkdnsrr("yahoo.fr");
print_r($resultat);

echo "<hr/>Obtienir les enregistrements DNS associ�s au nom d''h�te (dns_get_record): <br><pre>";
$resultat = dns_get_record("yahoo.fr");
print_r($resultat);
echo "</pre>";

echo "<hr/>Alias de checkdnsrr : <br>";
$resultat = dns_check_record("yahoo.fr");
print_r($resultat);

?>
