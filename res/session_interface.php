<?PHP
// https://meshcal.net

// CC BY SA - Jérôme Leignadier-Paradon (2015)
// Creative Commons 4.0
// http://creativecommons.org/licenses/by-sa/4.0/


/*	Afero GPL v3
	MeshCal - Calendar server (whis rfc 2445 and 5545)
    Copyright (C) 2015 Jérôme Leignadier-Paradon

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see :
	https://www.gnu.org/licenses/agpl.html
*/

// CeCILL 2.1 - Jérôme Leignadier-Paradon (2015)
// http://www.cecill.info/faq.fr.html

//------------------------------------------------------------------------------


//element de formulaire d'interface html pour les conexions d'utilisateur 


include("session_gestion.php");
$ret="";
//$ret= '<div id="session">';
if(!isset($_SESSION['log'])) {
	if(file_exists($rep_bases.'utilisateurs.csv')){
		$ret.= "<noscript>";
		$ret.=include("res/popup/formulaire_conexion.php");
		// declancher le popup de connection
		$ret.= "</noscript>".'<a href="javascript:void(0)" onclick="
			document.getElementById(\'formulaire_conexion\').style.display = \'block\';
			return false"><img class="inline" src="'.$rep_themes.$theme.'log_in_ico.png"  style="margin-right:7px" />Connexion</a>'
		;
	}
//si il y a un session existante donc si la session est ouverte
}else{
	// parametre du compte... (deconecter !)







	$ret.='	<ul class="menu_deroulant">
  				<li class="menu_deroulant">';

		/////////////////////////////////
		// mini avatard et identifiant //
		/////////////////////////////////
		$ret.= '<img class="inline" src="'.$rep_themes.$theme;
		$ret.= $_SESSION['niv']<=0?'avatar_SA.png':'';
		$ret.= $_SESSION['niv']==1?'avatar_administrateur.png':'';
		$ret.= $_SESSION['niv']==2?'avatar_organisateur.png':'';
		$ret.= $_SESSION['niv']==3?'avatar_editeur.png':'';
		$ret.= $_SESSION['niv']==5?'avatar_user.png':'';
		$ret.= $_SESSION['niv']==6?'avatar_banni.png':'';
		$ret.= '" width="10px height="13px" />&nbsp;';
		$ret.= $_SESSION['log'];
		$ret.= $_SESSION['niv']>=6?" (banni)":"";
    				
    	$ret.='	<ul class="menu_deroulant_2">';
















			//////////////////
			// Config perso //
			//////////////////
			$ret.='<li>&nbsp;';
				$ret.= '<form action="'.$query.'" method="post">';



				$ret.= '<input name="admin" value="" type="hidden">';
				$ret.= '<input type="hidden" name="admin_user" value="#perso">';

				$ret.= '<input type="image" id="configu" style="vertical-align: middle; margin-right:7px" class="inline" src="'.$rep_themes.$theme.'config.png" alt="Configuration personel" width="13px" height="13px" />';
				/* une image engrenage ou trois petites bares */
				$ret.= '<label for="configu" class="buster">Configuration</label>';
				//$ret.= '<input  type="submit" class="ghost">';
				$ret.= '</form>';
			$ret.='&nbsp;</li>';








			/////////////////
			// Deconecxion //
			/////////////////
			$ret.='<li>&nbsp;';
				$ret.= '<form action="'.$query.'" method="post">';
				$ret.= '<input name="logout" value="" type="hidden">';
				/* une image bouton off */
				$ret.= '<input type="image" id="decone" style="vertical-align: middle; margin-right:7px" class="inline" src="'.$rep_themes.$theme.'log_out_ico.png" alt="Déconexion" width="13px" height="13px" />';
								
				$ret.= '<label for="decone" class="buster">Déconnexion</label>';
				///$ret.= '<input id="decone" type="submit" class="ghost">';
				$ret.= '</form>';
			$ret.='&nbsp;</li>';

	

		$ret.=' </ul>
	</li></ul>';

}
//$ret.= '</div>';

return $ret;
?>
