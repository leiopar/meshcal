<?php
// https://meshcal.net

// CC BY SA - Jérôme Leignadier-Paradon (2015)
// Creative Commons 4.0
// http://creativecommons.org/licenses/by-sa/4.0/


/*	Afero GPL v3
	MeshCal - Calendar server (whis rfc 2445 and 5545)
    Copyright (C) 2015 Jérôme Leignadier-Paradon

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see :
	https://www.gnu.org/licenses/agpl.html
*/

// CeCILL 2.1 - Jérôme Leignadier-Paradon (2015)
// http://www.cecill.info/faq.fr.html

//------------------------------------------------------------------------------


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/* gestion des pages en cache pour soulager le serveur...  */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*
variable globale érité de "_config.php"
$rep_cache="cache/"; //repertoire zonne de cache
$cache_limit_nb_fic=100; //limitation du nombre de pages stocké dans le cache. (0 ==> 500)
$cache_limit_Moctet=10; //limitation de la taille du cache en megaoctets. (0 ==> pas de cache)

Les fichiers de cache sont appelés ainsi :
A-2016_fr.html			(Année 2016)
M-2016-3_fr.html		(mois de mars de 2016)
S-2016-15_fr.html		(semaine 15 de 2016)
J-2016-4-12_fr.html		(jour en date du 12/04/2016)
L_7_0-2016-4-12_fr.html	(liste "simple"...)



si il y à la moindre modification dans les calendiers locaux ou distant : vider le cache... sans pitier
Si une nouvelle page est généré, la stoquer dans le cache mais en supriment les ancien fichier de cache pour respecté les limites defini dan sle fichier "_config.php"

Quand un fichier de cache est utilisé, sa date de modification est mit à l'instant T.

tenir compte de la langue de l'utilisateur !!!

tenir compte du type de calendrier à afficher : unitaire ou metacalendrier

*/

















function nom_cache($tab_req){ // resolution du nom de fichier de cache à rechjercher ou à créer.
	global $langue, $metacal;

	if ($tab_req[0]=="A")$ret="A-".$tab_req[1];//annuel A-2016_fr
	if ($tab_req[0]=="M")$ret="M-".$tab_req[1]."-".$tab_req[2];//mensuel M-2016-3_fr
	if ($tab_req[0]=="S")$ret="S-".$tab_req[1]."-".$tab_req[2]."_".$langue;//ebdomadaire S-2016-15_fr
	if ($tab_req[0]=="J")$ret="J-".$tab_req[1]."-".$tab_req[2]."-".$tab_req[3];//jour J-2016-4-12_fr
	if ($tab_req[0][0]=="L"){//liste...
		$ret="L".$tab_req[1]."-".$tab_req[2]."-".$tab_req[3]."-".$tab_req[4]."-".$tab_req[5];//liste...
	}

	$ret.="_".$langue;

	$ret.="_".$metacal;

	return $ret; /* ajouter ".html" ? */
}


/* suppression de tout les fichier cache lié à un métacal */
function BOOMcache($metacal=""){

	/* fair en sort que seul les cache concerné par "$metacal" soit suprimé */
	global $rep_cache;
	if ($dir = opendir($rep_cache)) {
		while(($i = readdir($dir)) !== false) {   //le "!== false" c'est au cas un un repertoir s'apelle "0"(zéro)
			/* Là ça efface tout ce qu'il y a dans le repertoir de cache... peu etre envisager de ne suprimer que les "*.html" ? */
			if (!is_dir($rep_cache.$i))if($i!="erreurs.csv")if($i!="touch")unlink($rep_cache.$i);
			//on ne sprume bien que les fichier de cache mais pas les erreurs distantes
			/* peu etre faire un traitement spécial local/distant... */
		}
	}
}


function cache_limit_control(){ //suprime les plus vieux fichiers de cache jusqu'à ce que les limites soient respectées.
	global $rep_cache, $cache_limit_nb_fic, $cache_limit_Moctet, $developement;

	if($cache_limit_nb_fic>500 || $cache_limit_nb_fic<1)$cache_limit_nb_fic=500; //car limite du FAT16=[512 fichier par repertoire]

	//Listage des tout les fichiers de cache 
	// $tab([nom],[taille],[date])
	$nb=0;

	if ($dir = opendir($rep_cache)) {
		if($developement)echo "<hr />fichiers en cache :<br />";

		$tab = array();
		while(($i = readdir($dir)) !== false) {   //le "!== false" c'est au cas un un repertoir s'apelle "0"(zéro)
			if (!is_dir($rep_cache.$i) && (substr($i,0,1)!="."))
			if ($i!="erreurs.csv")if($i!="touch"){// si c'est bien un fichier de cache
				if($developement)echo "{".$i."}<br />";
				$nb++;
		  		$tab[] = array(// $tab([n(nom)],[t(taille)],[d(date)])
					"n" => $i,							//nom du fichier
					"t" => filesize($rep_cache.$i),	//taille du fichier
					"d" => filemtime($rep_cache.$i) //date de derniere modification du fichier
				);
			}
		}
		if($developement)echo "<hr />";

		if ($nb>=2){ // s'il y a au moins 2 fichier de cache...
			//tri de $fic par dates, les plus anciens en premier. et somme d'ocupation disque en octets
			$moa=array();//pour le tri...
			$totalo=0;//pour le calcul d'ocupation disque total
			foreach ($tab as $i => $row){
				$moa[$i]  = $row['d'];//date de dernière utilisation pour le tri...
				$totalo+=$row['t'];//totalisation du poids des fichiers...
			}
			array_multisort($moa, SORT_ASC, $tab);// pour trier du plus vieux au plus réssant
			//echo "<pre>"; print_r $tab; echo "</pre>";

			// determination si la limite de taille global du cache a été franchi... et traitement le cas echéant
			$cache_limit_octet=1024*1024*$cache_limit_Moctet;//conversion de Mo en octets.
			while ($totalo>$cache_limit_octet){
				$moa=array_shift($tab);//recuperation des info du fichier le plus vieux en les supriment du tableau de recencement
				unlink($rep_cache.$moa['n']);	//supression du plus vieux fichier...
				$totalo-=$moa['t'];
			}
			/* determination si la limite de nombre de fichier a été franchi... et traitement le cas echéant*/
			while ($nb > $cache_limit_nb_fic) {//tant que le nombre de fichier et superieur à la limite...
				$moa=array_shift($tab);//recuperation des info du fichier le plus vieux en les supriment du tableau de recencement
				unlink($rep_cache.$moa['n']);	//supression du plus vieux fichier...
				$nb--;
			}
		}
	}else{
		echo "il c'est passé un truc...";
	}
}


?>
