<?php
// https://meshcal.net

// CC BY SA - Jérôme Leignadier-Paradon (2015)
// Creative Commons 4.0
// http://creativecommons.org/licenses/by-sa/4.0/


/*	Afero GPL v3
	MeshCal - Calendar server (whis rfc 2445 and 5545)
    Copyright (C) 2015 Jérôme Leignadier-Paradon

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see :
	https://www.gnu.org/licenses/agpl.html
*/

// CeCILL 2.1 - Jérôme Leignadier-Paradon (2015)
// http://www.cecill.info/faq.fr.html

//------------------------------------------------------------------------------



//////////////////////////////////////////////////////////////////////////////////////////////
// edition des privilège d'ecriture dans les calendrier locaux utilisateurs par utilisateur //
//////////////////////////////////////////////////////////////////////////////////////////////
function edit_cal_user_perm($users_input,$cals_input,$act="majtt"){ //,$act="majtt"
	global $rep_bases, $metadonees_calendriers;

	//$users :
	//		"#tous" ==> appliquer à tout le monde
	//		"nom_utilisateur" ==> appliquer à l'utilisateur concerné
	//		array(user1, user2 ...) ==> appliquer au utilisateur de la liste
	if(is_array($users_input)){
		$users=$users_input;
	}else{
		$users[]=$users_input;
	}

	//$cals :
	//		"#tous" ==> à tout les calendrier
	//		"cal_n" ==> au calendrier "n"
	//		array(cal_1, cal_2 ...) ==> à la liste ...
	if(is_array($cals_input)){
		$calss=$cals_input;
	}elseif($cals_input=="#tous"){
		foreach($metadonees_calendriers as $i => $j)$cals[]=$j;
	}else{
		$cals[]=$cals_input;
	}

	$tnc="";	//tab num cal : liste abregé des calendrier (sans "cal_") pour etre intégré au fichier
	foreach($cals as $i)$tnc.=($tnc==""?"":";").substr($i,4);	//on ne garde que les numero sans "cal_" du debut

	//$act :
	//		"majtt"(ou rien) ==> mise a jour total (les calendreir présent dans le tableau sont ajouter, les absent suprimé)
	//		"suppr" ==> supression des acces au(x) calendrier(s) de $cals pour l'(es) utilisateur(s)
	//		"majcal"	==> Mise à jour pour un calendrier (les utilisateur présent sont autorisé, les absent interdi)

	//		"ajout" ==> ajout des acces ... (idem) ...

	// supression d'un calendrier necessite : edit_cal_user_perm("#tous",$cal,"suppr")
	// mise à jour des droit de tout les utilisateur pour un calendrier : edit_cal_user_perm($users,$cal,"majcal")

	$fich=@file_get_contents($rep_bases.'utilisateurs.csv');
	$tab_lignes=explode("\n", trim($fich)); //on separ le texte en lignes		

	$new_fich="";
	foreach($tab_lignes as $ligne)if((trim($ligne)!="")){ // pour chaque utilisateur
		$ligne==trim($ligne);
		if($ligne[0]!="#"){	//si c'est un commentaire alor la ligne repart telle quelle
			$tabu=explode("|",$ligne);
			if(in_array($tabu[0],$users) || in_array("#tous",$users) || $act=="majcal"){ // si l'utilisateur est concerné...;
				if($act=="majtt")$tabu[4]=$tnc; //on remplace tout
				if($act=="suppr" || $act=="majcal" || $act=="ajout"){ // on enlève les truc de $cals qui pourait trainer					
					$tabc=explode(";",$tabu[4]);		//liste des numero de calendrier ...
					$moa="";
					foreach($tabc as $i){	// pour chaque calendrier
						if(!in_array("cal_".$i,$cals))$moa.=($moa==""?"":";").$i; // on suprime les calendriers concernés
					}
					$tabu[4]=$moa;
				}

				if($act=="majcal" && in_array($tabu[0],$users)){	// on ajoute tout le cals
					$tabu[4].=($tabu[4]==""?"":";").$tnc;				
				}


				if($act=="ajout"){	// on ajoute tout les cals
					$tabu[4].=($tabu[4]==""?"":";").$tnc;				
				}
				$ligne=implode("|",$tabu);
			}
		}
		$new_fich.=($new_fich==""?"":"\n").$ligne;
	}
	if ($fich != $new_fich)file_put_contents($rep_bases.'utilisateurs.csv',$new_fich);
}








/* suppression de calendrier */
function supprime_cal($cal){
	global $rep_bases;

	/* supression de calendrier personel ??? */
	/* supression de metacalendrier ??? */

	global $metadonees_calendriers;
	//echo 'supression du calendrier "'.$metadonees_calendriers[$cal]["nom"].'"('.$cal.')';

	sup_cal_css($cal); // CSS
	sup_cal_base($cal);	// base/calendrier.csv

	if ($dir = opendir($rep_bases."metacal")) {	// metacal:*.csv
		$tab = array();
		while(($fich = readdir($dir)) !== false) {   //le "!== false" c'est au cas un un repertoir s'apelle "0"(zéro)
			if (!is_dir($rep_bases."metacal/".$fich) && (substr($fich,0,1)==".") && (substr($fich,-4)==".csv")){
				sup_cal_metacal($cal,$fich);
			}
		}
	}else{
		echo "il c'est passé un truc...";
	}
	
	if($metadonees_calendriers[$cal]["type"]=="local"){
		unlink($metadonees_calendriers[$cal]["adresse"]);	// calendrier.ics
		edit_cal_user_perm("#tous",$cal,"suppr"); // supression dans les autorisation utilisateur
	}
	BOOMcache();
	metalistage();	// Redefinir $metadonees_calendriers
}






///////////////////////////////////////////////////////
// crée une vouvelle entrée de couleur de calendrier //
///////////////////////////////////////////////////////
function new_cal_css($id,$coul){
	global $rep_cache;	
	$old_css=trim(file_get_contents($rep_cache."touch")); // ancien nom du CSS
	if($old_css=="")$old_css="couleurs_cal.css"; //pour retrocompatibilité...

	$new_css="couleurs_cal_".time().".css";				// nouveau nom du CSS.

	$contenu="";
	if(file_exists($old_css))$contenu=trim(file_get_contents($old_css))."\n";	// recupération de l'ancien fichier
	$contenu.=".".$id.'{color:'.$coul["text"].';background-color: '.$coul["fond"].';}'; //ajout... (class html)

	unlink(cal_css($old_css));								// Supression de l'ancien CSS
	file_put_contents($new_css, $contenu);			// creation du nouveau CSS avec le contenu
	file_put_contents($rep_cache."touch", $new_css);	// mise à jour du nom du CSS
}



/////////////////////////////////////////
// remplacer une couleur de calendrier //
/////////////////////////////////////////
function replace_cal_css($id,$coul){
	global $rep_cache;	
	$old_css=trim(file_get_contents($rep_cache."touch")); // ancien nom du CSS
	if($old_css=="")$old_css="couleurs_cal.css"; //pour retrocompatibilité...

	$new_css="couleurs_cal_".time().".css";				// nouveau nom du CSS.

	$contenu="";
	if(file_exists($old_css))$contenu=trim(file_get_contents($old_css));	// recupération de l'ancien fichier
	$fich=explode("\n",$contenu);
	$contenu="";
	foreach($fich as $ligne){
		$moa=explode("{",$ligne);
		if(isset($moa[1])){
			$contenu.=($contenu==""?"":"\n");
			if(trim($moa[0])!=".".$id){
				$contenu.=trim($ligne);
			}else{
				$contenu.=".".$id.'{color:'.$coul["text"].';background-color: '.$coul["fond"].';}'; //ajout... (class html)
			}
		}
	}

	unlink($old_css);								// Supression de l'ancien CSS
	file_put_contents($new_css, $contenu);			// creation du nouveau CSS avec le contenu
	file_put_contents($rep_cache."touch", $new_css);	// mise à jour du nom du CSS
}




//////////////////////////////////////////////////
// supprime une entrée de couleur de calendrier //
//////////////////////////////////////////////////
function sup_cal_css($id){
	global $rep_cache;	
	$old_css=trim(file_get_contents($rep_cache."touch")); // ancien nom du CSS
	if($old_css=="")$old_css="couleurs_cal.css"; //pour retrocompatibilité...

	$new_css="couleurs_cal_".time().".css";				// nouveau nom du CSS.

	$contenu="";
	if(file_exists($old_css))$contenu=trim(file_get_contents($old_css));	// recupération de l'ancien fichier
	$fich=explode("\n",$contenu);
	$contenu="";
	foreach($fich as $ligne){
		$moa=explode("{",$ligne);
		if(isset($moa[1]))if(trim($moa[0])!=".".$id) $contenu.=($contenu==""?"":"\n").trim($ligne);
	}

	unlink($old_css);								// Supression de l'ancien CSS
	file_put_contents($new_css, $contenu);			// creation du nouveau CSS avec le contenu
	file_put_contents($rep_cache."touch", $new_css);	// mise à jour du nom du CSS
}



////////////////////////////////////////////////////////
// suprime un entré de calendrier de la bases central //
////////////////////////////////////////////////////////
function sup_cal_base($id){
	global $rep_bases;
	$csv=$rep_bases."calendriers.csv";
	$contenu="";
	if(file_exists($csv))$contenu=trim(file_get_contents($csv));	// recupération de l'ancien fichier
	$fich=explode("\n",$contenu);
	$contenu="";
	foreach($fich as $ligne){
		$moa=explode("|",$ligne);
		if(isset($moa[1]))if(trim($moa[0])!=$id) $contenu.=($contenu==""?"":"\n").trim($ligne);
	}

	file_put_contents($csv, $contenu);			// creation du nouveau CSS avec le contenu
}



/////////////////////////////////////////////////////////////////////////////////////////
// supression de la reference d'un calendrier d'un affichage multiple (metacalendrier) //
/////////////////////////////////////////////////////////////////////////////////////////
function sup_cal_metacal($id,$metacal){
	global $rep_bases;
	$csv=$rep_base."metacal/".$metacal;
	$contenu=="";
	if(file_exists($csv))$contenu=trim(file_get_contents($csv));	// recupération de l'ancien fichier
	$fich=explode("\n",$contenu);
	$contenu=="";
	$BOOM=false;
	foreach($fich as $ligne){
		$moa=explode("|",$ligne);
		if(isset($moa[1]))if(trim($moa[0])!=$id){
			$contenu.=($contenu==""?"":"\n").trim($ligne);
		}else{
			$BOOM=true;
		}
	}
	
	if($BOOM)BOOMcache($metacal);
	file_put_contents($csv, $contenu);			// creation du nouveau CSS avec le contenu
}




































//------------------------------------------------------------------------------





////////////////////////////////////////////////////////////
// le nom proposé et il absent de la base de calendrier ? //
////////////////////////////////////////////////////////////
function nom_cal_nouveau($nom){
	global $metadonees_calendriers;
	$ret=true;
	$adresse=preg_replace('@[^a-zA-Z0-9_]@', '_', $nom).".ics";// même expression que pour construire le nom de fichier ics

	// tout en minuscule pour les testes !!!
	$adresse=strtolower($adresse);
	$nom=strtolower(trim($nom));
	$meta_min=array_change_key_case($metadonees_calendriers,CASE_LOWER);

	// testes...
	if(isset($meta_min[$nom]))$ret=false;// cherche si le nom est deja defini

	foreach($metadonees_calendriers as $i)if(strtolower($i["adresse"])==$adresse)$ret=false; //pour eviter les colision de fichier ics
	return $ret;
}



////////////////////////////////////////////////////////////////////////////
// Si l'URL proposé est presente dans la base, ça retourne le nom associé //
////////////////////////////////////////////////////////////////////////////
function url2nom($url){
	global $metadonees_calendriers;
	$ret="";
	foreach($metadonees_calendriers as $i => $j)
			if($j["type"]!="local")
					if($j["adresse"]==trim($url))
							$ret=$j["nom"];
	return $ret;
}



/////////////////////////////////////////////////////
// trouve le plus petit numero de calendrier libre //
/////////////////////////////////////////////////////
function num_cal_libre(){
	global $metadonees_calendriers;
	$i=1;
	while(isset($metadonees_calendriers["cal_".$i]))$i++;
	return ("cal_".$i);
}



///////////////////////////////////////////////////////////////////////////////////////////////////////
// Crée une liste en HTML des utilisateur à l'usage de l'interface d'administration (voir admin.php) //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function liste_user_admin_html(){
	global $tab_user, $rep_themes, $theme, $metadonees_calendriers;	

	$ret='<table>';
	$premier=true;
	$imp=true;
	foreach($tab_user as $nom => $user)if($nom!=$_SESSION["log"] && $user["niv"]>=$_SESSION["niv"]){

		$ret.="<tr".($imp?' class="imp" ':"").">";
			// mini avatar et nom
			$ret.="<td>";
				$ret.= isset($user["info_divers"])?'<span href="#" class="tooltip yellow-tooltip">':'';
				$ret.= '&nbsp;<img class="inline" src="'.$rep_themes.$theme;
				$ret.= $user["niv"]<=1?'avatar_administrateur.png':'';
				$ret.= $user["niv"]==2?'avatar_organisateur.png':'';
				$ret.= $user["niv"]==3?'avatar_editeur.png':'';
				$ret.= $user["niv"]==5?'avatar_user.png':'';
				$ret.= $user["niv"]==6?'avatar_banni.png':'';
				$ret.= $user["niv"]>=7?'avatar_banni.png':'';
				$ret.= '" width="10px height="13px" />&nbsp;';
				$ret.= $nom;
				$ret.= $user["niv"]=="6"?('('.ceil(($user["d_ban"]-time())/(24*60*60)).'J)'):'';/* controler quand banisement temporaire fini */
				$ret.= isset($user["info_divers"])?'<span>'.$user["info_divers"].'</span></span>':""; 
				$ret.='&nbsp;';
			$ret.="</td>";

			/* Pas encore ...
			// a un calendrier perso
			$ret.='<td'.($premier?' style="width:30px"':'').'>';
				if($user["cal_perso"])$ret.= '<img class="inline" src="'.$rep_themes.$theme.'cal_perso_ico.png" alt="A un calendrier personel" width="16px" height="13px" />';
			$ret.="</td>";
			*/

			// peu ecrir dans les calendrier
			foreach($metadonees_calendriers as $i => $j)if($j["type"]=="local"){
				$ret.='<td'.($premier?' style="width:18px"':'').'>';
				$moa=false;
				if($user["niv"]<=3)$moa=true;
				if(isset($user["cal_modif"]))if (array_key_exists($i, $user["cal_modif"]))$moa=true;
				$ret.=($moa?'<img class="inline" src="'.$rep_themes.$theme.'cal_modif_ico.png" alt="'.$i.'" width="16px" height="13px" />':"&nbsp;");
				$ret.="</td>";
			}

			// configuration
			$ret.='<td'.($premier?' style="width:30px"':'').'>';


			$ret.='<form method="POST" action="" name="form" class="inline">';
				$ret.='	<input name="admin" value="" type="hidden">
						<input type="hidden" name="admin_user" value="'.$nom.'">';

			$ret.= '&nbsp;<input type="image" style="vertical-align: middle;" class="inline" src="'.$rep_themes.$theme.'config.png" alt="modifier" width="13px" height="13px" />';

			$ret.='</form>'."\n";
			$ret.="</td>";
		$ret.="</tr>";

		$premier=false;
		$imp=!$imp;
	}
	$ret.="</table>";

	return $ret;
}



//------------------------------------------------------------------------------




///////////////////////////////////////
/*TODO reinitialisation de mots de passe */
///////////////////////////////////////
function reinit_mot_de_passe($log,$password,$adrcourriel,$langue){//couriel d'information de connexion
	global $courriel, $url_site; /*TODO metre un truc dans $courriel !!! */

	$headers  = "MIME-Version: 1.0\r\n";
	$headers .='Content-Type: text/plain; charset=utf-8'."\r\n"; // ici on envoie le mail au format texte encodé en UTF-8
	$headers .='Content-Transfer-Encoding: 8bit'; // ici on précise qu'il y a des caractères accentués


	$dest  = $adrcourriel;
	$titre = message(104,array(),$langue)." - ".$url_site; /***/
	$corp = message(106, /***/
		array(
			"nom" => $log ,
			"log"=> $_SESSION['log'],
			"pw" => $password,
			"url"=> $url_site//$_SERVER["HTTP_REFERER"] 
		),
		$langue)."<br/>";// récuperation du message selon la langue du destinataire
	mail($dest, $titre, utf8_decode($corp),"From:".$courriel."\n".$headers);
}























//////////////////////////////////////////////////////////////////
// Crée un tableau avec le fichier base de tout les utilisateur //
//////////////////////////////////////////////////////////////////
function charge_base_utilisateurs_complet(){
	global $rep_bases;
	$tab=array();

	$fichier=$rep_bases.'utilisateurs.csv';
	$csv = file ($fichier);// Lit une page web dans un tableau.
	$new ="";
	// Affiche toutes les lignes du tableau comme code HTML, avec les numéros de ligne
	foreach ($csv as $ligne)if(trim($ligne)!="" && trim($ligne)[0]!="#"){ //on ne tien pas compte des commentaires et ligne vide.
		$i= explode("|",trim($ligne));

		$tab[$i[0]]['courriel']=$i[2];

		if($i[3][0]=="6"){ // si bani temporairement
			$moa=explode("@",$i[3]);
			$tab[$i[0]]['niv']=6;
			if(time()>$moa[1]){
				$tab[$i[0]]["niv"]=5;

				// reconstituer la ligne de l'utilisateur !!!
				$moa=array("niv" => 5); reconstitu_utilisateur($i[0],$moa);
				
			}else{
				$tab[$i[0]]["d_ban"]=$moa[1];
			}
		}else{
			$tab[$i[0]]["niv"]=$i[3];
		}

		$moa=explode(";",$i[4]);
		foreach($moa as $j){
			if($j!="")$tab[$i[0]]['cal_modif']["cal_".$j]="";
		}
		//langue
		$tab[$i[0]]["lg"]=$i[5];

		//a son calendrier personel ?
		$tab[$i[0]]["cal_perso"]=($i[6]=="oui");

		//divers...
		if(isset($i[7]))if(trim($i[7])!="")$tab[$i[0]]["info_divers"]=trim($i[7]);
	}
	return $tab;
}




///////////////////////////////////////////////////////////////////////////////////////
// Crée un tableau avec certaine données d'un seul utilisateur pour l'administrateur //
///////////////////////////////////////////////////////////////////////////////////////
function charge_utilisateur_pour_admin($nom){
	global $rep_bases;
	$tab=array();
	$find=false;
	$csv = fopen($rep_bases.'utilisateurs.csv', 'rb');
	while ((($i = fgetcsv($csv, 1000, "|")) !== FALSE) && !$find)if($i[0][0]!="#"){ //on ne tien pas compte des commentaires.
		if($i[0]==$nom){
			$tab['courriel']=$i[2];

			if($i[3][0]=="6"){
				$moa=explode("@",$i[3]);
				$tab['niv']=6;
				if(time()>$moa[1]){
					$tab["niv"]=5;
				}else{
					$tab["d_ban"]=$moa[1];
				}
			}else{
				$tab["niv"]=$i[3];
			}
			
			$moa=explode(";",$i[4]);
			foreach($moa as $j){
				if($j!="")$tab['cal_modif']["cal_".$j]="";

			}
			//langue
			$tab["lg"]=$i[5];

			//a son calendrier personel ?
			$tab["cal_perso"]=($i[6]=="oui");

			//divers...
			if(isset($i[7]))if(trim($i[7])!="")$tab["info_divers"]=trim($i[7]);
			
			$find=true;
		}
	}
	fclose($csv);
	return $tab;
}



//////////////////////////////////////////////////////////////////////////
// refait la ligne d'un utilisateur et la réinsert dans le fichier base //
//////////////////////////////////////////////////////////////////////////
function reconstitu_utilisateur($nom,$tab){
	global $rep_bases;
	//echo "[reconstitu]";
	$fichier=$rep_bases.'utilisateurs.csv';
	$csv = file ($fichier);// Lit une page web dans un tableau.
	$new ="";
	// Affiche toutes les lignes du tableau comme code HTML, avec les numéros de ligne
	foreach ($csv as $ligne)if(trim($ligne)!=""){
		$cel=explode("|",$ligne);
		if($ligne[0]!="#" && $cel[0]==$nom){
			//On remplace les bouts fourni
			if(isset($tab["password"]))$cel[1]=$tab["password"];	//password [sha256]
			if(isset($tab["courriel"]))$cel[2]=$tab["courriel"];	//courriel
			if(isset($tab["niv"]))$cel[3]=$tab["niv"];				//niveau d'otaurisation global


				/* faire un implode() ici ? */
			if(isset($tab["cal_modif"]))$cel[4]=$tab["cal_modif"];	//liste des numéro de calendrier autorisé en ecriture
			if(isset($tab["lg"]))$cel[5]=$tab["lg"];				//langue
			if(isset($tab["cal_perso"])){							//calendrier perso ? ["oui" ou "non"]
				$cel[6]=($tab["cal_perso"]?"oui":"non");
				/* fonction qui créer ou suprime le cal perso... */
			}
			if(isset($tab["info_divers"]))$cel[7]=$tab["info_divers"];	//info divers...

			//et on refait la ligne qu'on empile... 
			$new.=trim(implode("|",$cel))."\r\n";

		}else{
			//sinon on remet la ligne telle quelle
			//echo trim($ligne)."\r\n";
			$new.=trim($ligne)."\r\n";
		}
	}
	//echo "<hr /><pre>".$new."</pre>";

	file_put_contents($fichier, $new);
}



//////////////////////////////////////////////////////////////
// prend un fichier, en extrait les evenements de façon sûr //
//////////////////////////////////////////////////////////////
function recompile_ics($input){
	$file=explode("\n",$input);
	$bevent=false;
	$ret="";
	$txt="";

	foreach($file as $lcal)if (trim($lcal)!=""){ //balayage du fichier ligne par ligne (si la ligne n'est pas vide !)

		if (substr($lcal, 0, 12) =="BEGIN:VEVENT"){ //debut d'event !!!
			//if ($ret!="")$txt.="\n";
			$bevent=true;
			$txt.="\nBEGIN:VEVENT";
		}elseif ($bevent){
			$txt.="\n".$lcal;
		}

		if (substr($lcal, 0 , 10) =="END:VEVENT"){ //fin de l'event : traitement...
			$ret.=$txt;
			$bevent=false;
			$txt="";
		}
	}
	return $ret;
}


///////////////////////////////////////////////////////////////////
// crée une liste des calendrier avec à l'usage d'administration //
///////////////////////////////////////////////////////////////////
function liste_cal_admin_html(){
	global $rep_themes, $theme, $metadonees_calendriers;	

	$ret='<table>';
	$premier=true;
	$imp=true;
	foreach($metadonees_calendriers as $num => $cal)if($cal["type"]=="local"){

		$ret.="<tr".($imp?' class="imp" ':"").">";
			// mini avatar et nom
			$ret.="<td>";
				$ret.= '<span href="#" class="tooltip yellow-tooltip">';
				$ret.= '&nbsp;<img class="inline" src="'.$rep_themes.$theme.'cal_modif_ico.png';
				$ret.= '" width="16px height="13px" />&nbsp;';
				$ret.= $cal["nom"];
				$ret.='<span>description ???</span>';

				$ret.='&nbsp;';
			$ret.="</td>";

			$ret.='<td>';
				$ret.= '&nbsp;<span class="'.$num.'">['.$num.']</span>&nbsp;';
			$ret.="</td>";


			// configuration
			$ret.='<td'.($premier?' style="width:30px"':'').'>';


			$ret.='<form method="POST" action="" name="form" class="inline">';
				$ret.='	<input name="admin" value="" type="hidden">

						<input type="hidden" name="admin_cal" value="'.$num.'">';

			$ret.= '&nbsp;<input type="image" style="vertical-align: middle;" class="inline" src="'.$rep_themes.$theme.'config.png" alt="modifier" width="13px" height="13px" />';

			$ret.='</form>'."\n";
			$ret.="</td>";
		$ret.="</tr>";

		$premier=false;
		$imp=!$imp;
	}
	/* calendriers distant */
	foreach($metadonees_calendriers as $num => $cal)if($cal["type"]!="local"){ //distant

		$ret.="<tr".($imp?' class="imp" ':"").">";
			// mini avatar et nom
			$ret.="<td>";
				$ret.= '<span href="#" class="tooltip yellow-tooltip">';
				$ret.= '&nbsp;<img class="inline" src="'.$rep_themes.$theme.'cal_net_ico.png';
				$ret.= '" width="16px height="13px" />&nbsp;';
				$ret.= $cal["nom"];
				$ret.='<span>description ???</span>';

				$ret.='&nbsp;';
			$ret.="</td>";

			$ret.='<td>';
				$ret.= '&nbsp;<span class="'.$num.'">['.$num.']</span>&nbsp;';
			$ret.="</td>";

			// configuration
			$ret.='<td'.($premier?' style="width:30px"':'').'>';


			$ret.='<form method="POST" action="" name="form" class="inline">';
				$ret.='	<input name="admin" value="" type="hidden">

						<input type="hidden" name="admin_cal" value="'.$num.'">';

			$ret.= '&nbsp;<input type="image" style="vertical-align: middle;" class="inline" src="'.$rep_themes.$theme.'config.png" alt="modifier" width="13px" height="13px" />';

			$ret.='</form>'."\n";
			$ret.="</td>";
		$ret.="</tr>";

		$premier=false;
		$imp=!$imp;
	}

	$ret.="</table>";

	return $ret;
}

?>
