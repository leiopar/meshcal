<?php
// https://meshcal.net

// CC BY SA - Jérôme Leignadier-Paradon (2015)
// Creative Commons 4.0
// http://creativecommons.org/licenses/by-sa/4.0/


/*	Afero GPL v3
	MeshCal - Calendar server (whis rfc 2445 and 5545)
    Copyright (C) 2015 Jérôme Leignadier-Paradon

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see :
	https://www.gnu.org/licenses/agpl.html
*/

// CeCILL 2.1 - Jérôme Leignadier-Paradon (2015)
// http://www.cecill.info/faq.fr.html

//-----------------------------------------------------------------------//


$MeshCal_vertion="00.24";

$url_site=""; // le meshcal par lui-même
if(isset($_SERVER["HTTP_REFERER"])){
	$url_site=trim(explode("?",$_SERVER["HTTP_REFERER"])[0],"/");
}else{// ne fonctione pas avec les serveur de teste en localhost...
	$url_site="http";
	if(isset($_SERVER["HTTPS"]))if($_SERVER["HTTPS"]=="on")$url_site.="s";
	$url_site.="://";
	$url_site.=$_SERVER["HTTP_HOST"]."/";
	$url_site.=trim(explode("?",$_SERVER["REQUEST_URI"])[0],"/")."/";
}
if($developement)echo "[HTTP_REFERER]=".$url_site."<hr />";

$url_home=""; // le site d'origine...

$title=$_SERVER["HTTP_HOST"]; // ce qu'il faut adfficher en premier dans le titre (va vers la  balise html "<title>"
//echo $url_site."<hr />";
$courriel="meshcal@".$_SERVER["HTTP_HOST"];	//adresse couriel aloué au serveur MeshCal(préremplissage bourin pour éviter certain bug avec la commande "mail()" de certain serveurs...)
$rep_cal=""; //repertoir ou sont les calendrier locaux
$rep_cal_sauv="sauvegardes/"; //repertoire de sauvegardes des calendrier locaux
$rep_cal_dist_sauv="sauvegardes/distant/"; //repertoir de sauvegarde des calendriers distants
$rep_bases="bases/";  //repertoire des bases de données générales
$rep_cache="cache/"; //repertoire zonne de cache
$rep_themes="themes/";
$css_disco=false; // A "true", ça fait des couleurs de text flachi... faut aimer :-/
$rep_perso="perso/"; //repertoire des calensrier perso

$langue="fr"; //langage par defaut

$d_ban=15;	//durée de ban temporaire par defaut en jours

$session_non_visite_temps_max=1; //temps(minutes) maximum de persistance de session entre deux requettes

$affichage_par_defaut="M";/* affichage par defaut lier au parametre $_GET['aff']
	//affichage annuel
A			//anné en cour
A-2016		//spécifiquement 2016 (à éviter !!! a cause de l'horison global)
A-2016-3	//préselection du mois de mars de 2016 (à éviter !!! a cause de l'horison global)
A-2016-3-12 //préselection du 12/03/2016 (à éviter !!! a cause de l'horison global)

	//affichage mensuel
M			//mois en cour
M-2016		//mois en cour reporté à 2016 (à éviter !!! a cause de l'horison global)
M-2016-3	//spécifiquement le mois de mars 2016 (à éviter !!! a cause de l'horison global)
M-2016-3-12 //préselection du 12/03/2016 (à éviter !!! a cause de l'horison global)

	//affichage ebdomadaire
S			//semaine en cour (ISO 8601:1988)
S-2016		//semaine en cour reporté à 2016 (à éviter !!! a cause de l'horison global)
S-2016-15	//spécifiquement la 15éme semaine de 2016 (à éviter !!! a cause de l'horison global)
S-2016-15-2 //affichage ebdomadaire (semaine numéro 15, deuxième jour de la semaine (mardi) (à éviter !!! a cause de l'horison global)

	//affichage sur une journée (avec un jour avant et un jour après)
J			// du jour actuel
J-2016		// du jour actuel reporté à 2016 (à éviter !!! a cause de l'horison global)
J-2016-4	// du jour actuel reporté à avril 2016 (à éviter !!! a cause de l'horison global)
J-2016-4-12 // du jour spécifique le 12/04/2016 (à éviter !!! a cause de l'horison global)

	//affichage par liste
L			// liste autour du jour actuel, par defaut 0 jours avant et 15 jours aprés.
L_20		// liste autour du jour actuel avec 20 jours après. (à éviter !!! a cause de l'horison global)
L_20_2		// liste autour du jour actuel evec 2 jours avant et 20 jours après. (à éviter !!! a cause de l'horison global)
L_20_2-2016-4-12	// liste avec 2 et 20 jours d'horison autour du 12/04/2016 (surtout pour les nom de fichier de cahe) (à éviter !!! a cause de l'horison global)
*/




//valeur des horisons par defaut en jours de l'affichage par liste 
$liste_horison_avant=0;
$liste_horison_apres=15;

//liste des alarme avantr evenement (voir RFC 2445), separatuer "|"
$event_rappel="-P3D|-PT3H|-PT3M";//" 3 jours, 3 heures et 3 minutes avant

$cache=true; //activation du cache (false, il n'y aura pas de cache, peu permetre à MeshCal de fonctioner sur des serveur bridé mais ralenti considérablement le systeme !

$metacal="~defaut"; //metacalendrier affiché (valeur par defaut)

//-----------------------------------------------------------------------//
//--------------------- limitations antiflood... ------------------------//
//-----------------------------------------------------------------------//

//valeur des horisons global en années de l'étendu possible d'affichage de tout type. 
$global_horison_avant=2;
$global_horison_apres=5;

//calmage du cache :
$cache_limit_nb_fic=50; //limitation du nombre de pages stocké dans le cache. (0 ==> 500 car limite du FAT16=[512 fichier par repertoire])
$cache_limit_Moctet=10; //limitation de la taille du cache en megaoctets. (0 ==> pas de cache)

$temps_mini_norm=0.5;			// durée en secondes admise entre deux navigation avant ralentisement volontaire (voir ip_somnolence.php)

$ics_distant_nb_max=50; 		/*nombre maximum de caledriers distant que l'on peux lier */
$ics_distant_sauv=true;			/*sauvegarder les calendrier distant en local */
$ics_distant_sauv_nb_max=5;		/*nombre maximum de version gardé en reserve (pour pouvoir permetre à l'admin de l'ICS distant de pouvoir récupéré ses données... */
$ics_distant_delta_control=24; //nombre d'heure minimum à laisser passer avant de recontroler les ICS distants.

$theme="_base/";		/* à modifier poure virer "$rep_theme" d'ici ! */	// theme par defaut



//-----------------------------------------------------------------------//
//---------------------------- Pour info... -----------------------------//
//-----------------------------------------------------------------------//

// $_SESSION['log'] : identifiant
// $_SESSION['cou'] : adresse couriel de l'utilisateur
// $_SESSION['niv'] : niveau d'autorisation global :
//		0 ==> super admin : comme l'administrateur mais inéfaçable
//		1 ==> administrateur : a acces en ecriture à tout les calendriers, à la liste des utilisateur et à leur adresse courriel
//		2 ==> organisateur : comme l'administrateur mais sans l'acces aux adressex couriel
//		3 ==> Editeur : a acces en ecriture à tout les calendriers
//		4 (indefini pour l'instant)
//		5 ==> Niveau par defaut : droit a rien sauf au calendrier autorisé par quelqu'un de niveau >=2
//		6@(timestamp) ==> bani temporaire (le banisement prend fin automatiquement au timestamp)
//		7 ==> bani definitif (tant qu'un utilisateur de niveai >=2 n'annule pas)


//-----------------------------------------------------------------------//
//Listage des variables de configuration (avec les valeurs par defaut):  //
//-----------------------------------------------------------------------//
//Utile pour "directiveomega.php", pour traiter le fichier de configuration perso et pour l'interface (future) de configuration superadmin.
$config_global=get_defined_vars();
//moins les trucs critiques hors code ($_POST $_GET ...)
foreach($config_global as $i => $j)if($i[0]=="_")unset($config_global[$i]);
//moins les truc de dev qui apparaissent dans "index.php"
unset($config_global['developement']);
unset($config_global['temps_de_traitement']);
//moins les trucs qu'il ne faut pas toucher...
unset($config_global['MeshCal_vertion']);
//moins les repertoires !!!
foreach($config_global as $i => $j)if(substr($i,0,4)=="rep_")unset($config_global[$i]);
//moins les machins qui ne servent pas encore à quelquechose
unset($config_global['theme']);
unset($config_global['langue']);
unset($config_global['metacal']);

if($developement){ // affichage de dev...
	echo "<pre>";
	print_r($config_global);
	echo "</pre>";
}

//-----------------------------------------------------------------------//
//------------------------ truc pour traitement -------------------------//
//-----------------------------------------------------------------------//
// (va se remplir tout seul au besoin... inutile de tripoter)

$metadonees_calendriers=array(); //liste des calendrier au format ICAL [nom], [adresse], [type], [contenu_fic], [style]...
$liste_evens_description=array(); //Detail des evenements ; une entrès par evenement ; rangé par "nom du calendrier"."|(tube)"."UID"
$liste_evens_occurences=array();//Tableau de transition avec toutes les dates contenant une occurence de tout les evenements pointant sur l'index du tableau "$liste_evens_description"
$erreurs=array();//tableau d'erreur à faire aparaitre en popup.
$affichage_erreur=false; //valeur par defaut d'affichage du tableau d'erreur.


//-----------------------------------------------------------------------//
//---------------- Récupération des parametre personalisé ---------------//
//-----------------------------------------------------------------------//

if (file_exists($rep_bases.'config.csv'))if ($csv = fopen($rep_bases.'config.csv', 'rb')){//s'il y a un fichier config perso
	while (($i = fgetcsv($csv, 1000, "=")) !== FALSE){
		
		//eval("\$moa = isset(\$".$i[0].");"); //petite sécurité pour eviter de creer des variable non prévu...(paranoïa=true)
		//if($moa){
		if(isset($config_global[$i[0]])){  // si la variable existe...	
			eval("\$".$i[0]." = \$i[1];"); // modification de la valeur par defaut
		}
	}
	fclose($csv);
}


?>
