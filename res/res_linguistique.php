<?php
// https://meshcal.net

// CC BY SA - J�r�me Leignadier-Paradon (2015)
// Creative Commons 4.0
// http://creativecommons.org/licenses/by-sa/4.0/


/*	Afero GPL v3
	MeshCal - Calendar server (whis rfc 2445 and 5545)
    Copyright (C) 2015 J�r�me Leignadier-Paradon

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see :
	https://www.gnu.org/licenses/agpl.html
*/

// CeCILL 2.1 - J�r�me Leignadier-Paradon (2015)
// http://www.cecill.info/faq.fr.html

//------------------------------------------------------------------------------


$langue="fr"; /** a rendre parametrable avec une variable de session */

//remplissage du tableau de message selon la langue selection�
	//Message par d�faut en francais (en redondance avec la routine suivante en cas de traduction incomplette)
$csv = fopen($rep_bases.'langues/'.$langue.'/dialogues.csv', 'rb'); // fichier de langue par d�faut
while (($i = fgetcsv($csv, 1000, ";")) !== FALSE) {
		$mess[$langue][$i[0]]=utf8_encode($i[1]); // table de la langue par d�faut
}
fclose($csv);
$messages=$mess[$langue];/*** remplacer tout les "$message" par des "$mess[$_SESSION['lng']]" */

function charge_langue($lg){//chargement d'une langue supl�mentaire
	global $mess, $langue, $rep_bases;
	$mess[$lg]=$mess[$langue]; // pr�chargement des message en francais (qui seront ensuite ecras�s s'il existe une corespondance dans la langue cible)
	
	$fich=$rep_bases.'langues/'.$lg."/dialogues.csv";
	if ($csv = fopen($fich, 'rb')){
		$mess[$lg]=$mess[$langue]; // pr�chargement des message en francais (qui seront ensuite ecras�s s'il existe une corespondance dans la langue cible)
		while (($i = fgetcsv($csv, 1000, ";")) !== FALSE) {
			$mess[$lg][$i[0]]=utf8_encode($i[1]);
		}
		fclose($csv);
	}
}


if (isset($_SESSION) && $_SESSION['lng']!=$langue  && $_SESSION['lng']!="")charge_langue($_SESSION['lng']); //pr�chargement de la langue de l'utilisateur si ce n'est pas celle par d�faut


function message($num,$tab_1d=array(),$lg=""){ /**/
	global $mess, $langue; /***/
	//echo "(message : ".$num." | langue : ".$lg.")";
	if (isset($_SESSION['lng'])){$slng=$_SESSION['lng'];}else{$slng=$langue;};
	if ($lg=="") $lg=(($slng!="")?$slng:$langue); //pour l'interface de connecsion...
	if(!isset($mess[$lg])) charge_langue($lg); // chrgement d'une langue supl�mentaire si ce n'est deja fait.
	$ret=$mess[$lg][$num];
	$ret= str_replace('\n',"\r\n",$ret);
	foreach($tab_1d as $i => $j) $ret= preg_replace("[\[#".$i."#\]]",$j,$ret);
	return $ret;
}

?>
