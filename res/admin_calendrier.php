<?php
// https://meshcal.net

// CC BY SA - Jérôme Leignadier-Paradon (2015)
// Creative Commons 4.0
// http://creativecommons.org/licenses/by-sa/4.0/


/*	Afero GPL v3
	MeshCal - Calendar server (whis rfc 2445 and 5545)
    Copyright (C) 2015 Jérôme Leignadier-Paradon

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see :
	https://www.gnu.org/licenses/agpl.html
*/

// CeCILL 2.1 - Jérôme Leignadier-Paradon (2015)
// http://www.cecill.info/faq.fr.html

//------------------------------------------------------------------------------



if($_SESSION['niv']<=3){// option accessible qu'au administrateurs, organisateurs et éditeurs
include("res/res_admin.php");
$type=$metadonees_calendriers[$_POST["admin_cal"]]["type"];



$ret='<link rel="stylesheet" href="'.$rep_themes.$theme.'admin.css" type="text/css" />'."\n";

/************************************/
/* tester niveau de l'operateur !!! */
/************************************/

if($type=="local" && $_SESSION['niv']<=2){
	$tab_user=charge_base_utilisateurs_complet(); 

	/////////////////////////////////
	// autorisation par calendrier //
	/////////////////////////////////
	// (traitement)
	if(isset($_POST["droits_users"])){
		$tab=array();
		foreach($tab_user as $nom => $j){
			if(isset($_POST["droit_".$nom])){
				$tab[]=$nom;
			}
		}
		//echo "<pre>";
		//print_r($tab);
		//echo "</pre>";	
		edit_cal_user_perm($tab,$_POST["admin_cal"],"majcal");
		//$modif_user["cal_modif"]=$moa;
		//$modif_user["modif"]=true;
	}


	$tab_user=charge_base_utilisateurs_complet(); // réapropriation pour que l'affichage soit à jour
}

//$tab_cal=charge_utilisateurs_pour_admin($_POST["admin_cal"]);
$modif_cal["modif"]=false;






////////////////////////////////////////////////
// interface d'administration d'un calendrier //
////////////////////////////////////////////////

$ret.='<br /><h2>Edition des parametre du calendrier "'.$metadonees_calendriers[$_POST["admin_cal"]]["nom"].'" :</h2>'."\n";













if($type=="local" && $_SESSION['niv']<=2){
	/////////////////////////////////
	// autorisation par calendrier //
	/////////////////////////////////
	// (formulaire)
	$ret.='<hr />&nbsp;<img class="inline" src="'.$rep_themes.$theme.'groupe_user_ico.png" alt="Droit des utilisateurs" width="16px" height="13px" />&nbsp;Droits d\'ecriture par utilisateur ce calendriers local :';
	$ret.='<form method="POST" action="" name="form" class="inline">';
	$ret.='<input name="admin" value="" type="hidden">';
	$ret.='<input type="hidden" name="admin_cal" value="'.$_POST["admin_cal"].'">';
	$ret.='<input name="droits_users" value="" type="hidden">';
	$ret.='<div id="utilisateurs"><table>';
	//$premier=true;
	$imp=true;
	foreach($tab_user as $nom => $user){

		$ret.="<tr".($imp?' class="imp" ':"").">";
			// mini avatar et nom
			$ret.="<td>";
				$ret.= '<span href="#" class="tooltip yellow-tooltip">';
				$ret.= '&nbsp;<img class="inline" src="'.$rep_themes.$theme;
				$ret.= $user["niv"]<=1?'avatar_administrateur.png':'';
				$ret.= $user["niv"]==2?'avatar_organisateur.png':'';
				$ret.= $user["niv"]==3?'avatar_editeur.png':'';
				$ret.= $user["niv"]==5?'avatar_user.png':'';
				$ret.= $user["niv"]==6?'avatar_banni.png':'';
				$ret.= $user["niv"]>=7?'avatar_banni.png':'';
				$ret.= '" width="10px height="13px" />&nbsp;';
				$ret.= $nom;
				$ret.='<span>'.$user["info_divers"].'</span>';
				$ret.= $user["niv"]=="6"?('('.ceil(($user["d_ban"]-time())/(24*60*60)).'J)'):'</span>';
				$ret.='&nbsp;';

			$ret.="</td>";
			$ret.="<td>";
				$ret.= '<input type="checkbox" name="droit_'.$nom.'"'.(isset($user["cal_modif"][$_POST["admin_cal"]])?" checked":"").'>'.$i;
			$ret.="</td>";

		$ret.="</tr>";

	//	$premier=false;
		$imp=!$imp;
	}
	$ret.="</table>";
	$ret.='<input type="submit" class="button" value="Metre les droits à jour..." /> ';
	$ret.="</div>";
	$ret.='</form>'."\n";
}










/* changement de couleur du calendrier */
// traitement
if($_POST["admin"]=="change_coul_cal"){
	$coul=rand_coul($metadonees_calendriers[$_POST['admin_cal']]["type"]=="local"?"sombre":"pale");
	replace_cal_css($_POST['admin_cal'],$coul);
	// remetre le CSS neuf
	$ret.='<link rel="stylesheet" href="'.$rep_cal.cal_css().'" type="text/css" />'."\n";
}


/////////////////////////////////////////
// changement de couleur du calendrier //
/////////////////////////////////////////
// formulaire
$ret.= '<hr />Couleur actuelle :&nbsp;<span class="'.$_POST['admin_cal'].'">['.$_POST["admin_cal"].']</span>&nbsp;';
$ret.= '<br />Tirer au sort une autre couleur : ';
$ret.='<form method="POST" action="" name="form" class="inline">';
	$ret.='	<input name="admin" value="change_coul_cal" type="hidden">';
	$ret.=' <input type="hidden" name="admin_cal" value="'.$_POST["admin_cal"].'">';
	$ret.= '<input type="submit" class="button" value="Changer..." /> ';
	$ret.=info("Change definitivement la couleur du calendrier en choisissant au azard.");
$ret.= '</form>';













//////////////////////////////
// Supression du calendrier //
//////////////////////////////

$ret.= '<hr />Suprimer le calendrier : ';
$ret.='<form method="POST" action="" name="form" class="inline">';
	$ret.='	<input name="admin" value="suppr_cal" type="hidden">
			<input type="hidden" name="cal" value="'.$_POST["admin_cal"].'">'; /* devra forcément etre autre chose... */
	$ret.= '<input type="checkbox" name="unlock_supr_1">';
	$ret.= '<input type="submit" class="button" value="Suprimer..." /> ';
	$ret.= '<input type="checkbox" name="unlock_supr_2">';
	$ret.=redalert("/!\ ATENTION /!\<br />
				Toutes les informations du calendrier ".'"'.$metadonees_calendriers[$_POST["admin_cal"]]["nom"].'"'." (les évenements) seront definitivement effacées !<br />
				Pour que l'effacement soit efectif, cochez les deux cases de part et d'autre du bouton [Suprimer...]");
$ret.= '</form>';


}else{
	$ret="acces interdi !!!";
}
return $ret;
?>
