<?php
// https://meshcal.net

// CC BY SA - Jérôme Leignadier-Paradon (2015)
// Creative Commons 4.0
// http://creativecommons.org/licenses/by-sa/4.0/


/*	Afero GPL v3
	MeshCal - Calendar server (whis rfc 2445 and 5545)
    Copyright (C) 2015 Jérôme Leignadier-Paradon

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see :
	https://www.gnu.org/licenses/agpl.html
*/

// CeCILL 2.1 - Jérôme Leignadier-Paradon (2015)
// http://www.cecill.info/faq.fr.html

//------------------------------------------------------------------------------


if(($nb_cal["locaux"]==0) || (!isset($_SESSION['log']))){ //Monkey_date ne peut etre invoqué s'il n'y a pas de calendrier local et si l'utilisateur n'est pas identifier
	unset($_POST["Monkey_Date"]);/* a modifier pour permetre passerelle en demandant de s'identifier dans Monkey_Date */
}else{
	if($_SESSION['niv']>5 )unset($_POST["Monkey_Date"]); // interdiction au bannis !!!
	/** fair en sorte que les droit soit trié par calendrier... */
}

if(isset($_POST["Monkey_Date"])){
	$corps=include("res/monkey_date_formulaire.php"); //Monkey... (formulaire de création/modification d'event
	$traiter_cal=false;
}


/* traitement de requette Monkey_Date */
if(isset($_POST["Monkey_Date_action"])){
	if($metadonees_calendriers[$_POST["cal_cible"]]["type"]=="local")if (droit_cal($_POST["cal_cible"])<=3){	/* control si pas banni entretemps !!! */ /* a l'avenir, traité la diférence entre créer et modifier */ // control des droit
		/*echo "<hr><pre>";
		print_r($_POST);
		echo "</pre><br>";*/
		
		//echo "[".$_POST["cal_cible"]."]{".$rep_cal.$metadonees_calendriers[$_POST["cal_cible"]]["adresse"]."}<br>";
		$cal_cible=$rep_cal.$metadonees_calendriers[$_POST["cal_cible"]]["adresse"];
		if(isset($_POST["cal_base"]) && $metadonees_calendriers[$_POST["cal_base"]]["type"]=="local")$cal_base=$rep_cal.$metadonees_calendriers[$_POST["cal_base"]]["adresse"];
		switch ($_POST["Monkey_Date_action"]) {
			case "creer":
				insert_event(POST2event_ical(),$cal_cible);	
			break;
			case "deriver":
				insert_event(POST2event_ical(),$cal_cible);				
			break;		
			case "modifier":
				if ($_POST["cal_base"]==$_POST["cal_cible"]){ //si c'est tout dans me même calendrier
					remplace_event($cal_cible, $_POST["id_base"],POST2event_ical());// on remplace...
				}else{ //si c'est d'un calendrier à un autre
					if($metadonees_calendriers[$_POST["cal_base"]]["type"]=="local")remplace_event($cal_base, $_POST["id_base"], "");//supression dans ancien calendrier
					insert_event(POST2event_ical(),$cal_cible);//creation dans le nouveau...
				}
			break;		
			case "suprimer":
				if($metadonees_calendriers[$_POST["cal_base"]]["type"]=="local")remplace_event($cal_cible, $_POST["id_base"], "");//supression...				
			break;
		}
		BOOMcache(); //supression du cache...



	}else{
		/** tentative de hack html malveillant ... à punir ! */
	}
}


?>
