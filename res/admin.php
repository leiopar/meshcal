<?php
// https://meshcal.net

// CC BY SA - Jérôme Leignadier-Paradon (2015)
// Creative Commons 4.0
// http://creativecommons.org/licenses/by-sa/4.0/


/*	Afero GPL v3
	MeshCal - Calendar server (whis rfc 2445 and 5545)
    Copyright (C) 2015 Jérôme Leignadier-Paradon

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see :
	https://www.gnu.org/licenses/agpl.html
*/

// CeCILL 2.1 - Jérôme Leignadier-Paradon (2015)
// http://www.cecill.info/faq.fr.html

//------------------------------------------------------------------------------



include("res/res_admin.php");

$ret='<link rel="stylesheet" href="'.$rep_themes.$theme.'admin.css" type="text/css" />'."\n";


/////////////////////////////////////////////////////////
// gestion des suppresions (calendrier et utilisateur) //
/////////////////////////////////////////////////////////
// traitement
if($_POST["admin"]=="suppr_user"){
	if (isset($_POST["unlock_supr_1"]) && isset($_POST["unlock_supr_2"])){
		if ($_SESSION["niv"]<=2 || $_SESSION["log"]==$_POST["user"]){
			supprime_user($_POST["user"]);
		}else{
			echo "tentative d'usurpation !! ";
		}
	}else{
		echo "la supression de l'utilisateur n'a pas ue lieu (toutes les cases de sécurité n'ont pas été cochées)"; 
	}
}
if($_POST["admin"]=="suppr_cal" && $_SESSION["niv"]<=3){
	if (isset($_POST["unlock_supr_1"]) && isset($_POST["unlock_supr_2"])){
		supprime_cal($_POST["cal"]);
	}else{
		echo "la supression du calendrier n'a pas ue lieu (totes les case de sécurité n'ont pas été cochées)"; 
	}
}


///////////////////////////
// Purge manuel du cache //
///////////////////////////
// traitement
if($_POST["admin"]=="BOOMcache" && $_SESSION["niv"]<=3)BOOMcache();






//------------------------------------------------------------------------------
$ret.='<br /><h2>Gestion des calendriers :</h2><br /><br />'."\n";

///////////////////////////////////////////////////////////////
// regenerre toutes les couleur (et reparrer le fichier CSS) //
///////////////////////////////////////////////////////////////
// traitement
if($_POST["admin"]=="regen_css" && $_SESSION["niv"]<=1){
	regenèrer_css(true);
}

////////////////////////////////////////////////////
// gestion des requette d'administration d'agenda //
////////////////////////////////////////////////////
// traitement
if(isset($_POST["admin_cal_action"])){ //s'il y à une action soumise pour les calendriers...

	//ajout_cal();


	include "res/res_admin_cal_creer.php";


}
// du coup, à tout hasard on remet à jour les couleurs...
$ret.='<link rel="stylesheet" href="'.$rep_cal.cal_css().'" type="text/css" />'."\n";


///////////////////////////////////////
// gestion calendrier par calendrier //
///////////////////////////////////////
$ret.='<u><h3>gestion des parametres calendrier par calendrier ';
$ret.=info("Pour accéder aux reglages fin d'un calendrier, cliquez sur l'image de petits engrnage corepondant.");
$ret.='</h3></u>'."\n";
$ret.=liste_cal_admin_html();

////////////////////////////////////////////
// Réapropriation des calendrier distants //
////////////////////////////////////////////
// formulaire
$ret.=recharge_cal_distant_form();



//-----------------------------------------------------------------------------------------
/////////////////////////////////////////////////////////
// Formulaire pour creer un ICS local à partir de rien //
/////////////////////////////////////////////////////////
$ret.='<br /><br />&nbsp;<img class="inline" src="'.$rep_themes.$theme.'cal_modif_ico.png" alt="'.$i.'" width="16px" height="13px" />&nbsp;';
$ret.='<u><h3>Creer d\'un nouveau calendrier local</h3></u>';
$ret.=info("Crée un calendrier vierge qui poura etre modifié ici (à condition d'avoir les droits nécésaires)");
$ret.='<form method="POST" action="" name="form" ><p>';
	$ret.='	<input name="admin" value="" type="hidden">
			<input type="hidden" name="admin_cal_action" value="creer_local">';// creer_local
	$ret.='Nom : <input name="nom" size="65" type="text" value="" />
			<input type="submit" class="button" value="Creer..." />';
$ret.='</p></form>'."\n";


///////////////////////////////////////////////////
// Formulaire pour acrochage d'URL d'ICS distant //
///////////////////////////////////////////////////
$ret.='&nbsp;<img class="inline" src="'.$rep_themes.$theme.'cal_net_ico.png" alt="'.$i.'" width="16px" height="13px" />&nbsp;';
$ret.='<u><h3>Lier un calendrier distant</h3></u>';
$ret.=info("Lier un calendrier ne fait qu'afficher un calendrier d'un autre site.<br />
On ne peut le modifier mais il se metra à jour tout seul toute les ".$ics_distant_delta_control." heures.");
$ret.='<form method="POST" action="" name="form" ><p>';
	$ret.='	<input name="admin" value="" type="hidden">
		<input type="hidden" name="admin_cal_action" value="lier_distant">';// lier_distant
	$ret.='Nom : <input name="nom" size="20" type="text" value="" />';
	$ret.='&nbsp;&nbsp;&nbsp;URL : <input name="url" size="30" type="text" value="(http://...)" />';
	$ret.=' <input type="submit" class="button" value="Ajouter..." />';
$ret.='</p></form>'."\n";


//////////////////////////////////////////////////////////////
// Formulaire pour importer un fichier en calendrier locale //
//////////////////////////////////////////////////////////////
$ret.='&nbsp;<img class="inline" src="'.$rep_themes.$theme.'rep_ico.png" alt="'.$i.'" width="16px" height="13px" />';
$ret.=' &rarr; <img class="inline" src="'.$rep_themes.$theme.'cal_modif_ico.png" alt="'.$i.'" width="16px" height="13px" />&nbsp;';
$ret.='<u><h3>Creer un calendrier local à partir d\'un fichier</h3></u>';
$ret.=info('Pour pouvoir importer un fichier, celui-ci doit respecter la RFC 2445 et ses mise à jours.<br />
Généralement ces fichier ont l\'extesion ".ics" ou ".ical".<br /> Le calendrier ainsi créer poura etre modifié ici (à condition d\'avoir les droits nécésaires)');
$ret.='<form method="POST" action="" name="form" enctype="multipart/form-data"><p>';
	$ret.='	<input name="admin" value="" type="hidden">';
	$ret.='	<input type="hidden" name="admin_cal_action" value="upload_fichier">';// upload_fichier
	$ret.='Nom : <input name="nom" size="20" type="text" value="" />';
	$ret.='&nbsp;&nbsp;&nbsp;Fichier : <input name="fichier" type="file" accept="text/*"/>';
	$ret.=' <input type="submit" class="button" value="Uploader..." />';
$ret.='</p></form>'."\n";


////////////////////////////////////////////////////////////////////////
// Formulaire pour importer un calendrier distant en calendrier local //
////////////////////////////////////////////////////////////////////////
$ret.='&nbsp;<img class="inline" src="'.$rep_themes.$theme.'cal_net_ico.png" alt="'.$i.'" width="16px" height="13px" />';
$ret.=' &rarr; <img class="inline" src="'.$rep_themes.$theme.'cal_modif_ico.png" alt="'.$i.'" width="16px" height="13px" />&nbsp;';
$ret.='<u><h3>Copier un calendrier distant en local</h3></u>';
$ret.=info("Importer un calendrier consiste à le copier d'un autre site pour le recréer ici.
Cela créer un calendrier local prérempli...<br />
Le lien avec le calendrier d'origine est rompu, donc les futures modifications faites sur le site d'origine ne serons pas repercutées.
Par contre le calendrier importer poura etre modifié ici (à condition d'avoir les droits nécésaires)");
$ret.='<form method="POST" action="" name="form" ><p>';
	$ret.='	<input name="admin" value="" type="hidden">
			<input type="hidden" name="admin_cal_action" value="importer_distant">';// importer_distant
	$ret.='Nom : <input name="nom" size="20" type="text" value="" />';
	$ret.='&nbsp;&nbsp;&nbsp;URL : <input name="url" size="30" type="text" value="(http://...)" />';
	$ret.=' <input type="submit" class="button" value="Importer..." />';
$ret.='</p></form>'."\n";








/* // pour l'instant on met de coté...
// creation de metacalendrier 
if(isset($_POST["admin_cal_action"]))if($_POST["admin_cal_action"]=="creer_metacal"){
	echo "creer_metacal";
	/*** tester si doublon !!!
}


$ret.='&nbsp;<img class="inline" src="'.$rep_themes.$theme.'metacal_ico.png" alt="'.$i.'" width="16px" height="13px" />&nbsp;';
$ret.='<u><h3>Creer une selection de calendrier</h3></u>';
$ret.=info('Créer une selection (ou "métacalendrier") permet de faire des affichages plus ciblés que l\'affichage par defaut qui, lui, est exaustif.');
$ret.='<form method="POST" action="" name="form" ><p>';
	$ret.='	<input name="admin" value="" type="hidden">
			<input type="hidden" name="admin_cal_action" value="creer_metacal">';// creer_metacal
	$ret.='Nom : <input name="nom" size="65" type="text" value="" />
			<input type="submit" class="button" value="Creer..." />';
$ret.='</p></form>'."\n";
*/


























//------------------------------------------------------------------------------------------









///////////////////////////////////////////////////////////////
// regenerre toutes les couleur (et reparrer le fichier CSS) //
///////////////////////////////////////////////////////////////
// formulaire
if ($_SESSION["niv"]<=1){
	$ret.='<br /><u><h3>Regénérer toutes les couleurs des calendriers :</h3></u>'."\n";
	$ret.='<form method="POST" action="" name="form" class="inline">';
		$ret.='	<input name="admin" value="regen_css" type="hidden">';
		$ret.= '<input type="checkbox" name="unlock_regen_css_1">';
		$ret.= '<input type="submit" class="button" value="Rebrasser..." /> ';
		$ret.= '<input type="checkbox" name="unlock_regen_css_2">';
		$ret.=redalert("/!\ ATENTION /!\<br />
					Toutes les couleur seront définitivement modifiées !<br />
					Pour que le changement soit efectif, cochez les deux cases de part et d'autre du bouton [Rebrasser...]");
	$ret.= '</form><br />';
}



///////////////////////////
// Purge manuel du cache //
///////////////////////////
// formulaire
if ($_SESSION["niv"]<=3){
	$ret.='<br /><br />';
	$ret.='<u><h3>Suprimer le cache html</h3></u>';
	$ret.=info("Elimine tout les affichages précalculés de calendriers.");
	$ret.='<form method="POST" action="" name="form" ><p>';
		$ret.='	<input name="admin" value="BOOMcache" type="hidden">';
		$ret.='<input type="submit" class="button" value="Purger..." />';
	$ret.='</p></form>'."\n";
}






$ret.='<br /><br /><br />';



//-------------------------------------------------------------------------------------------//

//////////////////////////////////
//////////////////////////////////
//// Gestion des utilisateurs ////
//////////////////////////////////
//////////////////////////////////

if ($_SESSION["niv"]<=2){
	$ret.='<hr /><h2>&nbsp;<img class="inline" src="'.$rep_themes.$theme.'groupe_user_ico.png" alt="'.$i.'" width="16px" height="13px" /> Gestion des utilisateurs :</h2><br /><br />'."\n";

	/////////////////////////
	// ajout d'utilisateur //
	/////////////////////////
	$ret.='<u><h3>Ajout d\'utilisateur(s)</h3></u>'."\n";
	$ret.=include("res/res_admin_inscription.php");

	/////////////////////////////////////////////////////////////
	// lien vers gestion des droit utilisateur par utilisateur //
	/////////////////////////////////////////////////////////////
	$ret.='<br /><br /><u><h3>gestion des droits des utilisateurs ';
	$ret.=info("Pour accéder aux reglages fin d'un utilisateur, cliquez sur l'image de petits engrnage corepondant.");
	$ret.='</h3></u>'."\n";
	$tab_user=charge_base_utilisateurs_complet();
	$ret.='<div id="utilisateurs">';
	$ret.=liste_user_admin_html();// Listage, acces individuel et résumé des utilisateurs
	$ret.="</div>";

	// ça fait doublon actuelement...
	// $ret.='<hr><a href="'.$query.'" >Sortir de l\'interface d\'administration et retourner au calendrier...</a>'."\n";
}

return $ret;
?>
