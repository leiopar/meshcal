<?PHP
// https://meshcal.net

// CC BY SA - Jérôme Leignadier-Paradon (2015)
// Creative Commons 4.0
// http://creativecommons.org/licenses/by-sa/4.0/


/*	Afero GPL v3
	MeshCal - Calendar server (whis rfc 2445 and 5545)
    Copyright (C) 2015 Jérôme Leignadier-Paradon

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see :
	https://www.gnu.org/licenses/agpl.html
*/

// CeCILL 2.1 - Jérôme Leignadier-Paradon (2015)
// http://www.cecill.info/faq.fr.html

//------------------------------------------------------------------------------



/////////////////////////
// point d'information //
/////////////////////////
function info($text){
	global $rep_themes, $theme;
	$ret='<span href="#" class="tooltip info-tooltip">';
	$ret.='&nbsp;<img class="inline" src="'.$rep_themes.$theme.'info_ico.png" width="13px height="13px" />';
	$ret.='<span>'.$text.'</span>';
	$ret.='</span>';
	return $ret;
}
function redalert($text){
	global $rep_themes, $theme;
	$ret='<span href="#" class="tooltip info-tooltip">';
	$ret.='&nbsp;<img class="inline" src="'.$rep_themes.$theme.'alert.png" width="13px height="13px" />';
	$ret.='<span>'.$text.'</span>';
	$ret.='</span>';
	return $ret;
}




////////////////////////////////////////////
// Réapropriation des calendrier distants //
////////////////////////////////////////////
// formulaire
function recharge_cal_distant_form(){
	global $ics_distant_delta_control;
	$ret='<form method="POST" action="" name="form" style="display:inline;">';
		$ret.='	<input name="recontrol_cal_distant" value="" type="hidden">';
	
		$ret.='<input id="reload" style="vertical-align: middle;" class="inline" src="themes/_base/reload_ico.png" alt="Recharger tout les calendriers distants." type="image" >';

		$ret.='	<label for="reload" class="buster">Recharger...</label>';

		$ret.=info('Les calendrier distants sont normalement recontrolé toute les '.$ics_distant_delta_control.' heures.<br />
					"Recharger..." permet de forcer ce contrôl sans atendre.<br />
					Cette opération ne peut etre répété à moins de 30 secondes d\'intervale.');
	$ret.='</form>'."\n";

	return $ret;
}




//////////////////////////////////////////////////////////////////////////////////////////////
// verifi si l'adresse couriel est "sufisament valide" (ce n'est pas une science exact !!!) //
//////////////////////////////////////////////////////////////////////////////////////////////
function verif_courriel($courriel){
// Auteur : bobocop (arobase) bobocop (point) cz
// Traduction des commentaires par mathieu http://www.developpez.net/forums/u20527/mathieu/

// Le code suivant est la version du 2 mai 2005 qui respecte les RFC 2822 et 1035
// http://www.faqs.org/rfcs/rfc2822.html
// http://www.faqs.org/rfcs/rfc1035.html

	$atom   = '[-a-z0-9!#$%&\'*+\\/=?^_`{|}~]';		// caractères autorisés avant l'arobase
	$domain = '([a-z0-9]([-a-z0-9]*[a-z0-9]+)?)';	// caractères autorisés après l'arobase (nom de domaine)
                               
	$regex = '/^' . $atom . '+' .   				// Une ou plusieurs fois les caractères autorisés avant l'arobase
			'(\.' . $atom . '+)*' .         		// Suivis par zéro point ou plus
													// séparés par des caractères autorisés avant l'arobase
			'@' .                           		// Suivis d'un arobase
			'(' . $domain . '{1,63}\.)+' .  		// Suivis par 1 à 63 caractères autorisés pour le nom de domaine
													// séparés par des points
			$domain . '{2,63}$/i';          		// Suivi de 2 à 63 caractères autorisés pour le nom de domaine

// test de l'adresse e-mail
	return (preg_match($regex, $courriel));
}



////////////////////////////////////////////////////////////////////////
// Generateur de mot de passe... à modifier pour rendre plus complexe //
////////////////////////////////////////////////////////////////////////
function nouveau_mot_de_pass_base(){
	$pw='';$p1='';$p2='';$p3='';
	$c = 'bcdfghjkmnprstvwzBCDFGHJKLMNPRSTVWZbcdfghjkmnprstvwz';
	$v = 'aeiouyAEUYaeiouaeiou';
	$l = $c.$v.$v;
	$s ='+-*_&?+-';

	for($i=0;$i < 2; $i++){
		$p1 .= $c[rand(1, strlen($c))-1];
		if (rand(0,2)==1){$p1 .= $l[rand(1, strlen($l))-1];}
		$p1 .= $v[rand(1, strlen($v))-1];
	}
	$p2 = rand(10,999);
	$p3=$s[rand(1, strlen($s))-1];

	switch (rand(1,6)) {
		case 1: $pw = rtrim($p1.$p2.$p3);break;
		case 2: $pw = rtrim($p1.$p3.$p2);break;
		case 3: $pw = rtrim($p2.$p1.$p3);break;
		case 4: $pw = rtrim($p2.$p3.$p1);break;
		case 5: $pw = rtrim($p3.$p1.$p2);break;
		case 6: $pw = rtrim($p3.$p2.$p1);break;
	}
	return $pw;
}


///////////////////////////////////////////////////////////////
// Donne le nom du fichier CSS des couleurs de calendrier    //
//		(bidouillage nécésaire avec timestamp pour forcer le //
//		navigateur à prendre en compte les modifications...) //
///////////////////////////////////////////////////////////////
function cal_css(){
	global $rep_cache;
	$nom_css=file_get_contents($rep_cache."touch");
	if($nom_css=="")$nom_css="couleurs_cal.css"; //pour retrocompatibilité...
	return file_get_contents($rep_cache."touch");
}



/////////////////////////////////////////
// deffini une couleur aléatoir en RVB //
/////////////////////////////////////////
function rand_coul($type){
	global $css_disco;
	if($type=="pale"){
		$co[1]=155;			$co[2]=255;			$co[3]=rand(155,255);
	}elseif($type=="flash"){
		$co[1]=0;			$co[2]=255;			$co[3]=rand(0,255);
	}elseif($type=="sombre"){
		$co[1]=0;			$co[2]=127;			$co[3]=rand(0,127);
	}

	// qu'elles soient claires ou pas les couleurs sont volontairement contrastées au maximum
	// car le gris sera pour les calendriers "perso"
	$rd[1]=rand(1,3);
	if ($rd[1]==1){	
		if(rand(1,2)==1){	$rd[2]=2;$rd[3]=3;	}else{	$rd[2]=3;$rd[3]=2;	}
	}elseif ($rd[1]==2){
		if(rand(1,2)==1){	$rd[2]=1;$rd[3]=3;	}else{	$rd[2]=3;$rd[3]=1;	}
	}elseif ($rd[1]==3){
		if(rand(1,2)==1){	$rd[2]=2;$rd[3]=1;	}else{	$rd[2]=1;$rd[3]=2;	}
	}

	$rouge=dechex($co[$rd[1]]);if(strlen($rouge)== 1)$rouge="0".$rouge;
	$vert=dechex($co[$rd[2]]);if(strlen($vert)== 1)$vert="0".$vert;
	$bleu=dechex($co[$rd[3]]);if(strlen($bleu)== 1)$bleu="0".$bleu;
	$coul["fond"]="#".$rouge.$vert.$bleu;
	
	if($css_disco){
	// efficace mais ca peu piquer les yeux !
		$rouge=dechex(255-$co[$rd[1]]);if(strlen($rouge)== 1)$rouge="0".$rouge;
		$vert=dechex(255-$co[$rd[2]]);if(strlen($vert)== 1)$vert="0".$vert;
		$bleu=dechex(255-$co[$rd[3]]);if(strlen($bleu)== 1)$bleu="0".$bleu;
		$coul["text"]="#".$rouge.$vert.$bleu;
	}else{
		// plus sobre...
		if ($type=="sombre"){
			$coul["text"]="white";
		}elseif($type=="pale"){
			$coul["text"]="dimgrey";
		}elseif($type=="flash") {
			$coul["text"]="black";
		}
	}

	return $coul;
}

/////////////////////////////////////////////////
// Regénère toutes les couleurs des calendrier //
/////////////////////////////////////////////////
function regenèrer_css($total=false){
	global $metadonees_calendriers, $rep_cal, $rep_cache;

	$old_css=$rep_cal.trim(file_get_contents($rep_cache."touch"));
	$new_css="couleurs_cal_".time().".css";				// nouveau nom du CSS.
	$lignes=explode("\n",trim(file_get_contents($old_css)));
	$contenu="";
	//echo "<pre>";
	//print_r($lignes);
	//echo "<hr /></pre>";

	foreach($lignes as $l){
		$i=explode("{",$l);
		$cal=substr(trim($i[0]),1);
		if(isset($metadonees_calendriers[$cal]))$metadonees_calendriers[$cal]["css"]="{".trim($i[1]);
	}

	foreach($metadonees_calendriers as $i => $c){

		$coul=rand_coul($c["type"]=="local"?"sombre":"pale");
		$moa="{color:".$coul["text"].";background-color:".$coul["fond"].";}";
		if(!isset($c["css"]))$c["css"]=$moa;
		if($total)$c["css"]=$moa;
		$contenu.=($contenu==""?"":"\n").".".$i.$c["css"];
	}

	//echo "<pre>";
	//echo $contenu;
	//echo "<hr /></pre>";
	
	unlink($old_css);								// Supression de l'ancien CSS
	file_put_contents($new_css, $contenu);			// creation du nouveau CSS avec le contenu
	file_put_contents($rep_cache."touch", $new_css);	// mise à jour du nom du CSS
}


/* suppression d'utilisateur */
function supprime_user($user){
	global $rep_bases;
	$fichier=$rep_bases.'utilisateurs.csv';
	$csv = file ($fichier);// Lit une page web dans un tableau.
	$new_fich="";
	foreach($csv as $ligne)if(trim($ligne)!=""){ // pour chaque utilisateur
		$tabu=explode("|",$ligne);
		if($tabu[0]!=$user || $tabu[3]==0)$new_fich.=($new_fich==""?"":"\n").trim($ligne); // si l'utilisateur est concerné...
	}
	file_put_contents($fichier, $new_fich);

	/* metacal:*.csv */
	/* calendrier personel */
	/* CSS ??? */

}

/*modification/création d'une variable de configuration*/
function maj_config($nom, $val){ //nom et valeur de variable
	global $rep_bases;
	$fichier=$rep_bases.'config.csv';
	$tab=array();
	if ($csv = fopen($fichier, 'rb')){//lecture fichier de base de données des utilisateur
		while (($i = fgetcsv($csv, 1000, "=")) !== FALSE){
		
			// j'ignore si c'est vraiment utile...
			//if (is_numeric($i[1])){
			//	$i[1]=$i[1]+1;
			//	$i[1]=$i[1]-1;	
			//}

			eval("\$moa = isset(\$GLOBALS['".$i[0]."']);"); //petite sécurité pour eviter de creer des variable non prévu...(paranoïa=true)
			if($moa){
				//eval("\$".$i[0]." = \$i[1];");
				$tab[$i[0]]=$i[1];
				echo $i[0]."=".$i[1]."<br />";
			}
		}
		fclose($csv);
	}

	$tab[$nom]=$val; //ajout/modif de la variable concerné

	/*recréation du fichier*/
	$new_fich="";
	foreach($tab as $n => $v) $new_fich.="$n=$v\r\n";

	file_put_contents($fichier, $new_fich);
}

//------------------------------------------------------------------------------

////////////////////////////////////////////
////////////////////////////////////////////
////                                    ////
////  Pour creer un compte utilisateur  ////
////                                    ////
////////////////////////////////////////////
////////////////////////////////////////////

function list_calendriers_html(){ //liste de calendrier local*/
	global $metadonees_calendriers;/***/
	$ret="";
	foreach($metadonees_calendriers as $i => $j)if($j["type"]=="local"){
		if($ret!="")$ret.="<br />";
		$ret.=$i." ".$j["nom"];
	}
	return $ret;
}


function test_identifiant_existant($login){
	global $tab_user; /***/
	echo "<pre>";
		print_r($tab_user);
	echo "<pre>";

	foreach($tab_user as $i => $j){
		if (strtoupper($i)==strtoupper($login)){
			return true;
			break;		}
	}
	return false;
}


function charge_base_utilisateurs(){
	global $rep_bases;
	$tab=array();
	$csv = fopen($rep_bases.'utilisateurs.csv', 'rb');
	while (($i = fgetcsv($csv, 1000, "|")) !== FALSE)if($i[0][0]!="#" && $i[0]!=""){
		$tab[]=array('log'=>$i[1], 'cou'=>$i[2]); /*TODO comment ce machin là peu il marcher ??? ce doit etre du code mort ! */
	}
	fclose($csv);
	return $tab;
}


function ajout_dans_fic($ligne,$fichier){/*TODO A généraliser !!! */
	if(!file_exists($fichier)){
		//creation du fichier encore inexistant.
		$old = umask(0);
		$csv = fopen($fichier,"w");
			fwrite($csv, $ligne);
		fclose($csv);
		chmod($fichier, 0766);
		umask($old);
	}else{
		//ajout d"une ligne
		$csv = fopen($fichier,"a");
			fwrite($csv,"\n".trim($ligne));//fwrite($csv,"\r\n".$ligne);
		fclose($csv);
	}
}


// Envoi des info de coinexion (sans traces dans la base)

function courriel_info_connexion($log,$password,$adrcourriel,$langue){//couriel d'information de connexion
	global $courriel, $url_site; /*TODO metre un truc dans $courriel !!! */



	$headers  = "MIME-Version: 1.0\r\n";
	$headers .='Content-Type: text/plain; charset=utf-8'."\r\n"; // ici on envoie le mail au format texte encodé en UTF-8
	$headers .='Content-Transfer-Encoding: 8bit'; // ici on précise qu'il y a des caractères accentués

	/*TODO patch*/$courriel="pierre_deverel@laposte.net";

	$quifait=(isset($_SESSION['log'])?$_SESSION['log']:"Serveur Meshcal");

	$dest  = $adrcourriel;
	$titre = message(104,array(),$langue)." - ".$url_site; /***/
	$corp = message(105, /***/
		array(
			"nom" => $log ,
			"log"=> $quifait,
			"pw" => $password,
			"url"=> $url_site//$_SERVER["HTTP_REFERER"] 
		),
		$langue)."<br/>";// récuperation du message selon la langue du destinataire
	mail($dest, $titre, utf8_decode($corp),$headers."\r\n"."From:".$courriel);

	$inscrit=$log;
	if(isset($_SESSION['log'])){
		mail( //courier de confirmation à l'inscripteur
			$_SESSION['cou'], (message(104)." - ".$inscrit." => ".$url_site),
			utf8_decode(message(97,array( /***/
				"nom" =>$inscrit,
				"log"=>$_SESSION['log'],
				"cou"=>$adrcourriel
			))),
			$headers."Bcc:".$courriel."\r\n"."From:".$courriel."\r\n"
		);
	}
}


function compil_liste_erreur($csv){ /***TODO integrer le tableau général d'erreur à la place */
	$tab = array();
	$tab=explode(",",trim($csv,","));
	$ret="";
	//$ret=$csv."<br/>";/***/
	foreach($tab as $i){
		if ($i!="96") $ret.= message($i)."<br/>";
	}
	return $ret;
}


//------------------------------------------------------------------------------

?>
