<?php

/* ajout d'un calendrier... */

//global $rep_bases, $rep_cache, $rep_cal, $rep_cal_dist_sauv, $erreurs, $MeshCal_vertion;

$err=false;
$contenu_ics="";

$type="local"; // par defaut on crée un ICS local
if($_POST["admin_cal_action"]=="lier_distant")$type="distant"; //... sauf dans ce cas là !





//////////////////////////////////////
// teste de validité du nom proposé //
//////////////////////////////////////
if(isset($_POST["nom"])){
	$nom=trim($_POST["nom"]);
	if($nom==""){
		$erreurs[]=array("type"=>'pas de nom donné !');	//message d'erreur
		$err=true;
	}else{
		if(!nom_cal_nouveau($nom)){ // (test si il n'y a pas déja un calenbdrier portant ce nom)
			$erreurs[]=array("type"=>'Nom proche deja présent dans la base');	//message d'erreur
			$err=true;
		}
		if(strlen($nom)>65){
			$erreurs[]=array("type"=>'Nom trop long (ne doit pas dépasser 65 caractaires)');	//message d'erreur
			$err=true;
		}
	}
}





///////////////////////////////
// test de validité de l'URL //
///////////////////////////////
if(isset($_POST["url"])){
	$url=trim($_POST["url"]);
	if($url=="" || $url=="(http://...)"){
		$erreurs[]=array("type"=>"pas d'adresse renseigné !");	//message d'erreur
		$err=true;
	}elseif(filter_var($url, FILTER_VALIDATE_URL)!=$url){
		//echo "[".filter_var(trim($_POST["url"]), FILTER_VALIDATE_URL)."]";
		$erreurs[]=array("type"=>"Adresse invalide !");	//message d'erreur
		$err=true;
	}else{
		$moa=url2nom($_POST["url"]);// si ça ne renvoye pas "" c'est que cette URL existe deja dans la base
		if($moa==""){
			$url=trim($_POST["url"]);
		}else{
			$erreurs[]=array("type"=>'calendrier distant deja indexé sous ce nom : "'.$moa.'"');	//message d'erreur
			$err=true;
		}
	}

	if(!$err){
		$contenu_ics=trim(@file_get_contents($url)); //chargement du contenu ICS brut
		if($contenu_ics===false){// si le fichier est indispo...
			$erreurs[]=array("type"=>"url_nouveau_cal_distant_ne_repond_pas", "url_cal"=>$url);	//message d'erreur
			$err=true;
		}
	}
}




/////////////////////////////////////////////////////////////////////
// Teste de présence d'un fichier uploadé et s'i est bien non vide //
/////////////////////////////////////////////////////////////////////
$moa=false;
if(isset($_FILES['fichier']['tmp_name'])){
	/* tester si c'est bien un "*.ics" !!! (et pas un "*.exe", "*.php" ...) */
	if($_FILES['fichier']['tmp_name']==""){
		$moa=true;
	}else{
		$contenu_ics=trim(file_get_contents($_FILES['fichier']['tmp_name'])); //chargement du contenu ICS brut
		if($contenu_ics=="")$moa=true;
	}
}
if($moa){
	$erreurs[]=array("type"=>"pas de fichier transmis !");	//message d'erreur
	$err=true;
}





//////////////////////////////////////////////
// Teste si le fichier recu est bien un ics //
//////////////////////////////////////////////
if($contenu_ics!="" && $_POST["admin_cal_action"]!="creer_local"){
	$contenu_ics_tab=explode("\n", $contenu_ics);
	if(trim($contenu_ics_tab[0])!="BEGIN:VCALENDAR"){ /*/ a rendre plus securitaire/sofistiqué ? ou pas ? */  //tester si source est bien un ICAL
		$erreurs[]=array("type"=>"Le fichier proposé n'est pas un calendrier ICAL/ICS !",); //message d'erreur
		$err=true;
	}
	/*TODO teste à faire à chaque mise à jour des calendrier distant ! */


	if(!$err){ /* récupération des info des fichiers importés (uploadés, downloadés ou distants) */


		//on atrape toutes les info d'entète au passage...
		foreach ($contenu_ics_tab as $lcal){ //balayage du fichier ligne par ligne (si la ligne n'est pas vide !)
			if(substr($lcal,0,1)==" "){ // les info peuvent rendre plusieurs lignes. Les suivantes sont toujours précédé d'un espace.
				$entete_ics_tab[$rec[0]].=trim($lcal);
			}else{
				$rec = array();
				$rec = explode(':',$lcal); // parsage

				// DESCRIPTION, URL et SUMMARY peuvent contenir des ":" !!!
				$entete_ics_tab[$rec[0]]=trim(substr($lcal,strlen ($rec[0])+1));
			}
			if (substr($lcal, 0, 12) =="BEGIN:VEVENT"){ //debut d'un event, donc c'est que c'est la fin de l'entete de fichier.
				break;
			}
		}
	
	//echo "<pre>";
	//print_r ($entete_ics_tab);
	//echo "</pre><hr />";
		
	}
}







// recuperation d'ics distant pour liaison
if ($type=="distant" && !$err){ 
	$adresse=trim($_POST["url"]); // l'url est testé en amont*** tester l'url ! */
	$nom_local=preg_replace('@[^a-zA-Z0-9_]@', '_', $adresse);

	file_put_contents($rep_cal_dist_sauv.$nom_local.".ics", $contenu_ics); //enregistrement dans sauvegarde...

	$hash=hash('md4', $contenu_ics);
	file_put_contents($rep_cal_dist_sauv.$nom_local.".ics".".hash", $hash."\n".time()."|".date("Y-m-d G:i:s")); // somme de somme de control du fichier distant...
	


// sinon, construction du calendrier local 
}elseif(!$err){//..."local"
	$nom=str_replace('|','&#124;',trim($_POST["nom"]));//supression des "|" à cause des bases.csv.
	$adresse=preg_replace('@[^a-zA-Z0-9_]@', '_', $nom).".ics";

	$fich="BEGIN:VCALENDAR\n";
	$fich.="VERSION:2.0\n";
	$fich.="PRODID:-//MeshCalLike/".$MeshCal_vertion."//(autre truc optionel... site)\n"; /***/
	$fich.="X-WR-CALNAME:".$nom."\n";



	/***TODO ajouter description... */
	/* c'est là : 
		$entete_ics_tab["X-WR-CALNAME"]
		$entete_ics_tab["X-WR-CALDESC"]
	*/
	


	$fich.="X-WR-TIMEZONE:Europe/Paris\n";/* à internationaliser... */
	$fich.="CALSCALE:GREGORIAN";
	/*$fich.="X-WR-CALDESC:(description optionelle)\n"; */

	
	// importation ...
	if($_POST["admin_cal_action"]=="importer_distant"){// importation d'un calendrier d'un autre site
/**/		if($moa=file_get_contents($url)){
			$moa=recompile_ics($moa); /* faire en sorte d'extraire la description et le nom original */
		}else{
			$moa="";
			$err=true;
			echo "Le fichier distant ne semble pas accesible ou n'a pas de contenu.";
		}
	}elseif($_POST["admin_cal_action"]=="upload_fichier"){// importation par upload d'un fichier
/**/		$moa=recompile_ics(file_get_contents($_FILES['fichier']['tmp_name']));
	}
	$fich.=$moa; /* rien ou le contenu expurgé du fichier calendreier importé */


	$fich.="\nEND:VCALENDAR";
	file_put_contents($rep_cal.$adresse, $fich); // création du calendrier proprement dit !		
	$hash=hash('md4', $fich);
	//file_put_contents($rep_cal.$adresse.".hash", $hash."\n".time()."|".date("Y-m-d G:i:s")); // somme de control du fichier ...
}












if(!$err){ // etape final de création... si tout c'est bien passé avant !
	$id=num_cal_libre();
	$coul=rand_coul($type=="local"?"sombre":"pale");
	
	// ajour dans couleurs_cal.css
	$old_css=$rep_cal.trim(file_get_contents($rep_cache."touch"));
	$new_css="couleurs_cal_".time().".css";				// nouveau nom du CSS.

	$contenu=trim(file_get_contents($old_css));
	$contenu.="\n.".$id.'{color:'.$coul["text"].';background-color: '.$coul["fond"].';}';

	unlink($old_css);								// Supression de l'ancien CSS
	file_put_contents($new_css, $contenu);			// creation du nouveau CSS avec le contenu
	file_put_contents($rep_cache."touch", $new_css);	// mise à jour du nom du CSS

	
	// ajout dans bases/calendriers.csv
	$fichier=$rep_bases."calendriers.csv";
	$contenu=trim(file_get_contents($fichier));
	$contenu.="\n".$id.' | '.$nom.' | '.$adresse.' | '.$type.' |';



	/***TODO ajouter description... */
	/* c'est là : 
		$entete_ics_tab["X-WR-CALNAME"]
		$entete_ics_tab["X-WR-CALDESC"]
	*/



	file_put_contents($fichier, $contenu);



	/* metre dans le metacal par defaut $rep_bases."metacal/~defaut.csv" */
	$fichier=$rep_bases."metacal/~defaut.csv";
	$contenu=trim(file_get_contents($fichier));
	$contenu.="\n".$id;
	file_put_contents($fichier, $contenu);
	
	metalistage();
	if($_POST["admin_cal_action"]!="creer_local")BOOMcache();
}else{
	$affichage_erreur=true; // forcage de l'affichage de la popup de message d'erreurs
}






?>
