<?php
// https://meshcal.net

// CC BY SA - Jérôme Leignadier-Paradon (2015)
// Creative Commons 4.0
// http://creativecommons.org/licenses/by-sa/4.0/


/*	Afero GPL v3
	MeshCal - Calendar server (whis rfc 2445 and 5545)
    Copyright (C) 2015 Jérôme Leignadier-Paradon

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see :
	https://www.gnu.org/licenses/agpl.html
*/

// CeCILL 2.1 - Jérôme Leignadier-Paradon (2015)
// http://www.cecill.info/faq.fr.html

//------------------------------------------------------------------------------


include("res/res_admin.php");


if($_POST["admin_user"]=="#perso"){
	$post_user=$_SESSION['log'];
	$perso=true;
}else{
	$post_user=$_POST["admin_user"];
	$perso=false;
}


$user_cible=charge_utilisateur_pour_admin($post_user);
if ($developement){
	echo "\$user_cible<pre>";
	print_r($user_cible);
	echo "</pre><hr />";
}
$modif_user["modif"]=false;

//////////////////////////////////////////////////////////////////////////////////////////////////////
// Securisation : un utilisateur ne peux pas modifier un autre autilisateur au privilèges supérieur //
//////////////////////////////////////////////////////////////////////////////////////////////////////
if($_SESSION['niv']>$user_cible["niv"]){
	echo "tentative d'usurpation de niveau !!! ";
	$ret="";
}else{







////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
//// interface d'administration des statut d'un utilisateur ////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

$ret='<link rel="stylesheet" href="'.$rep_themes.$theme.'admin.css" type="text/css" />'."\n";



$ret.='<br /><h2>Edition des parametre utilisateur de '.$post_user.' :</h2>'."\n";




////////////////////////////
// modification du niveau //
////////////////////////////
if($_SESSION['niv']<=2 && $user_cible["niv"]>0 && !$perso){ //accesible uniquement au administrateurs et organisateurs en respectant une hiérarchie sauf pour l'utilisateur super_admin et le conmpte personel

	// traitement
	if(isset($_POST["niv_user"]))if($_SESSION['niv']<=$_POST["niv_user"]) {
		if($_POST["niv_user"]==6){
			// tempo de ban temporaire...
			$moa="6@".($_POST["d_ban"]*24*60*60+time());
		}else{
			$moa=$_POST["niv_user"];
		}
		$user_cible["niv"]=$_POST["niv_user"];
		$modif_user["niv"]=$moa;
		$modif_user["modif"]=true;
		// et pour que le formulaire soit à jour, on réécrit et recharge l'utilisateur cible...
		reconstitu_utilisateur($post_user,$modif_user);
		$user_cible=charge_utilisateur_pour_admin($post_user);

	}else{
		echo "Tentative d'usurpation de niveau !";
	}




	// formulaire
	$ret.= '<hr />&nbsp;Niveau d\'autorisation : <br />';
	$ret.='<form method="POST" action="" name="form" class="inline">';
		$ret.='	<input name="admin" value="" type="hidden">
				<input type="hidden" name="admin_user" value="'.$post_user.'">';



		$ret.= '<input type="radio" name="niv_user" value="1" id="niv_user_1"'.($user_cible["niv"]==1?' checked="checked" ':' ').($_SESSION['niv']>1?"disabled ":" ").'>';
		$ret.= '<label for="niv_user_1">';
		$ret.= '<img class="inline" src="'.$rep_themes.$theme.'avatar_administrateur.png" width="10px height="13px" />&nbsp;';
		$ret.='administrateur.</label>';
		$ret.=info("Un administrateur général peu gerrer tout les utilisateurs et les calendriers.<br />
					/!\ Il a acces aux adresses mail de tout les utilisateurs.");


		$ret.= '<br /><input type="radio" name="niv_user" value="2" id="niv_user_2"'.($user_cible["niv"]==2?' checked="checked"':'').($_SESSION['niv']>2?"disabled ":" ").'>';
		$ret.= '<label for="niv_user_2">';
		$ret.= '<img class="inline" src="'.$rep_themes.$theme.'avatar_organisateur.png" width="10px height="13px" />&nbsp;';
		$ret.='Organisateur</label>';
		$ret.=info("Un organisateur peu gerrer tout les calendriers et les autorisations qui y sont liées.<br />
					/!\Il a acces à la liste de tout les utilisateur.");


		$ret.= '<br /><input type="radio" name="niv_user" value="3" id="niv_user_3"'.($user_cible["niv"]==3?' checked="checked"':'').($_SESSION['niv']>3?"disabled ":" ").'>';
		$ret.= '<label for="niv_user_3">';
		$ret.= '<img class="inline" src="'.$rep_themes.$theme.'avatar_editeur.png" width="10px height="13px" />&nbsp;';
		$ret.='Editeur</label>';
		$ret.=info("Un editeur peu gerrer editer dans tout les calendrier sauf les personel.<br />
					Il n'a pas acces à la liste des utilisateur.");


		$ret.= '<br /><input type="radio" name="niv_user" value="5" id="niv_user_5"'.($user_cible["niv"]==5?' checked="checked"':'').'>';
		$ret.= '<label for="niv_user_5">';
		$ret.= '<img class="inline" src="'.$rep_themes.$theme.'avatar_user.png" width="10px height="13px" />&nbsp;';
		$ret.='Utilisateur</label>';
		$ret.=info("Niveau par defaut. Un utilisateur ne peu editer que les calendrier pour lequel il a reçu une autorisation spécifique.");


		$ret.= '<br /><input type="radio" name="niv_user" value="6" id="niv_user_6"'.($user_cible["niv"]==6?' checked="checked"':'').'>';
		$ret.= '<label for="niv_user_6">';
		$ret.= '<img class="inline" src="'.$rep_themes.$theme.'avatar_banni.png" width="10px height="13px" />&nbsp;';
		$ret.='Bani pendant </label>';
		$ret.= '<input name="d_ban" type="number" step="1" value="';
			$ret.=(isset($user_cible["d_ban"])?ceil(($user_cible["d_ban"]-time())/(24*60*60)):$d_ban);
		$ret.='" min="1"> jour(s)';
		$ret.=info("Un bani temporaire ne retrouvera automatiquement ses droit entérieur qu'à la fin de la période défini.");


		$ret.= '<br /><input type="radio" name="niv_user" value="7" id="niv_user_7"'.($user_cible["niv"]==7?' checked="checked"':'').'>';
		$ret.= '<label for="niv_user_7">';
		$ret.= '<img class="inline" src="'.$rep_themes.$theme.'avatar_banni.png" width="10px height="13px" />&nbsp;';
		$ret.='Bani indéfiniment</label>';
		$ret.=info("Un tel bani le reste jusqu'à une décision d'annulation sa faveur d'un administrateur ou d'un organisateur. A defaut sa situation est definitive.");

		/* prevenir en cas d'auto edition car ça ne peu etre qu'un retrogradage de droit !!! */
		$ret.='<br /><input type="submit" class="button" value="Modifier le statut de '.$post_user.'" /> ';


	$ret.= '</form>';

}








if($_SESSION['niv']<=1){// option accessible qu'au administrateur

	/* reinitialiser le mot de passe ! */
	// traitement
	if(isset($_POST["reinit_pw"])) {
		$moa=nouveau_mot_de_pass_base();
		$modif_user["password"]=hash("sha256",$moa);
		$modif_user["modif"]=true;
		reinit_mot_de_passe($post_user,$moa,$user_cible["courriel"],$user_cible["lg"]);
		/* envoyer le nouveau mot de passe à l'interressé... */
		/* avec une variente de la fonction existante (dans "res_admin_inscription.php") : */
		/* courriel_info_connexion($log,$password,$adrcourriel,$langue) */
	}
	// formulaire
	$ret.= '<hr />&nbsp;Recréer un nouveau mot de passe ';
	$ret.='<form method="POST" action="" name="form" class="inline">';
		$ret.='	<input name="admin" value="" type="hidden">
				<input type="hidden" name="admin_user" value="'.$post_user.'">';
		$ret.='	<input name="reinit_pw" value="" type="hidden">';
		$ret.=' <input type="submit" class="button" value="Réinitialiser..." /> ';
		$ret.=info("Un nouveau mot de passe sera envoyé à ".$post_user.".");
		//$ret.=info("Un message d'information sera envoyé à l'ancienne et la nouvelle adresse.");
	$ret.= '</form>';


	//////////////////////
	// adresse courriel //
	//////////////////////
	if(isset($_POST["courriel"])) if($modif_user["courriel"]!=$_POST["courriel"]) {
		if(verif_courriel($_POST["courriel"])){ //control de la validité de l'adresse
			$user_cible["courriel"]=$_POST["courriel"];
			$modif_user["courriel"]=$_POST["courriel"];
			$modif_user["modif"]=true;
			/* envoyer un message d'erreur */
		}else{
			echo "[erreur couriel]";
			/* sinon, message d'erreur et pas de modif */
		}
	}

	$ret.= '<hr />&nbsp;Adresse courriel : ';
	$ret.='<form method="POST" action="" name="form" class="inline">';
		$ret.='	<input name="admin" value="" type="hidden">
				<input type="hidden" name="admin_user" value="'.$post_user.'">';
		$ret.= '<input type="text" name="courriel" value="'.$user_cible["courriel"].'" >';
		$ret.= ' <input type="submit" class="button" value="Modifier..." /> ';
		//$ret.=info("Un message d'information sera envoyé à l'ancienne et la nouvelle adresse.");
	$ret.= '</form>';


}

/* pas encore...
///////////////////////////
// Calendrier personel ? //
///////////////////////////
if(isset($_POST["cal_perso"])){
	$modif_user["cal_perso"]=($_POST["cal_perso"]=="oui");
	$user_cible["cal_perso"]=$modif_user["cal_perso"];
	$modif_user["modif"]=true;
}

$ret.= '<hr />&nbsp;<img class="inline" src="'.$rep_themes.$theme.'cal_perso_ico.png" alt="A un calendrier personel" width="16px" height="13px" />&nbsp;Calendrier personel ?';
$ret.='<form method="POST" action="" name="form" class="inline">';
	$ret.='	<input name="admin" value="" type="hidden">
			<input type="hidden" name="admin_user" value="'.$post_user.'">';
	$ret.= '<input type="radio" name="cal_perso" value="oui" id="cal_perso_oui"'.( $user_cible["cal_perso"]?' checked="checked"':'').'><label for="cal_perso_oui">Oui</label> ; ';
	$ret.= '<input type="radio" name="cal_perso" value="non" id="cal_perso_non"'.(!$user_cible["cal_perso"]?' checked="checked"':'').'><label for="cal_perso_non">Non</label>';
	$ret.= ' <input type="submit" class="button" value="Valider..." /> ';
	$ret.=info("Un calendrier personel est visible par tous mais n'est modifiable que par son propriétaire.<br />
			Il se comporte comme un calendrier local mais il n'est pas intégré par defaut dans l'affichage principal.<br />
			Cependant, on peut l'y ajouter ainsi que dans n'importe quel métacalendrier.");
$ret.= '</form>';

*/











/////////////////////////////////
// autorisation par calendrier //
/////////////////////////////////
// traitement
if($_SESSION['niv']<=2){
	if(isset($_POST["droits_cal"])){
		$moa="";
		foreach($metadonees_calendriers as $i => $j)if($j["type"]=="local"){
			unset($user_cible["cal_modif"][$i]);
			if(isset($_POST[$i])){
				$moa.=($moa==""?"":";").substr($i,4);
				$user_cible["cal_modif"][$i]="";
			}
		}
		$modif_user["cal_modif"]=$moa;
		$modif_user["modif"]=true;
	}





	$ret.='<hr />&nbsp;<img class="inline" src="'.$rep_themes.$theme.'cal_modif_ico.png" alt="Droit calendriers" width="16px" height="13px" />&nbsp;Droits d\'ecriture sur calendriers locaux :';
	$ret.='<form method="POST" action="" name="form" class="inline">';
		$ret.='	<input name="admin" value="" type="hidden">';
		$ret.=' <input type="hidden" name="admin_user" value="'.$post_user.'">';
		$ret.='	<input name="droits_cal" value="" type="hidden">';	
	$ret.='<div id="droit_cal"><table>';
	$imp=true;
	foreach($metadonees_calendriers as $i => $j)if($j["type"]=="local"){ // liste des calendrier locaux précoché
		$ret.="<tr".($imp?' class="imp" ':"").">";
			// mini avatar et nom
			$ret.="<td>";
				$ret.= '<input type="checkbox" name="'.$i.'"'.(isset($user_cible["cal_modif"][$i])?" checked":"").'>('.$i.')';
				$ret.= $j["nom"];
			$ret.="</td>";
		$ret.="</tr>";
		$imp=!$imp; //prour griser une ligne sur deux
	}
	$ret.="</table></div>";
	$ret.='<input type="submit" class="button" value="Metre les droits à jour..." /> ';
	$ret.='</form>'."\n";

}


















/////////////////////////////////
// supression de l'utilisateur //
/////////////////////////////////
// formulaire (pour le traitement, voir "admin.php"
$ret.= '<hr />Suprimer l\'utilisateur : ';
$ret.='<form method="POST" action="" name="form" class="inline">';
	$ret.='	<input name="admin" value="suppr_user" type="hidden">
			<input type="hidden" name="user" value="'.$_POST["admin_user"].'">'; /* devra forcément etre autre chose... */
	$ret.= '<input type="checkbox" name="unlock_supr_1">';
	$ret.= '<input type="submit" class="button" value="Suprimer..." /> ';
	$ret.= '<input type="checkbox" name="unlock_supr_2">';
	$ret.=redalert("/!\ ATENTION /!\<br />
				Toutes les informations de l'utilisateur ".'"'.$_POST["admin_user"].'"'." (adresse courriel, calendrier personel...) seront definitivement effacées !<br />
				Pour que l'effacement soit efectif, cochez les deux cases de part et d'autre du bouton [Suprimer...]");
$ret.= '</form>';







////////////////////////////////////////////////////////////////
// reconstruction de la l'igne de base de l'utilisateur cible //
////////////////////////////////////////////////////////////////
if($modif_user["modif"]){
	//echo "<pre>";
	//print_r ($modif_user);
	//echo "</pre>";
	reconstitu_utilisateur($post_user,$modif_user);
}




}return $ret;
?>
