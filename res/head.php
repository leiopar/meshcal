<?php
// https://meshcal.net

// CC BY SA - J�r�me Leignadier-Paradon (2015)
// Creative Commons 4.0
// http://creativecommons.org/licenses/by-sa/4.0/


/*	Afero GPL v3
	MeshCal - Calendar server (whis rfc 2445 and 5545)
    Copyright (C) 2015 J�r�me Leignadier-Paradon

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see :
	https://www.gnu.org/licenses/agpl.html
*/

// CeCILL 2.1 - J�r�me Leignadier-Paradon (2015)
// http://www.cecill.info/faq.fr.html

//------------------------------------------------------------------------------


/* * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                 */
/*                    ent�te html                  */
/*                                                 */
/* (appell� directement par le fichier "index.php" */
/* * * * * * * * * * * * * * * * * * * * * * * * * */

header('Content-Type: text/html; charset=UTF-8');

$ret='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">'."\n";
$ret.='<?xml version="1.0" encoding="UTF-8"?>'."\n";
$ret.='<html xmlns="http://www.w3.org/1999/xhtml">'."\n";
$ret.='<head>'."\n";
$ret.='<title>'.$title.' - MeshCal</title>'."\n";
$ret.='<!-- M�tadon� de d�scription !!! -->'."\n";
$ret.='<meta name="description" content="description en prose" />'."\n";
$ret.='<meta name="keywords" lang="fr" content="mots, clefs" />'."\n";
$ret.='<meta name="identifier-url" content="http://nom_du_site.fr/" />'."\n";
$ret.='<meta name="robots" content="index,follow" />'."\n";
$ret.='<meta http-equiv="content-language" content="fr" />'."\n";
$ret.='<meta name="Language" content="French" />'."\n";
$ret.='<meta name="coverage" content="France" />'."\n";
$ret.='<meta name="geo.country" content="fr" />'."\n";
$ret.='<meta name="author" content="J�r�me LEIGNADIER-PARADON, leiopar@free.fr" />'."\n";
$ret.='<meta name="category" content="categorie, celons, clasification, des, annuaires, en, ligne" />'."\n";
$ret.='<meta name="MSSmartTagsPreventParsing" content="TRUE" />'."\n";
$ret.='<meta http-equiv="Content-type" content="text/html; charset=utf-8"/>'."\n";
$ret.='<link rel="shortcut icon" href="'.$rep_themes.$theme.'favicon.png" /> <!-- image � creer !!! -->'."\n";
$ret.='<link rel="stylesheet" href="'.$rep_themes.$theme .'base.css" type="text/css" />'."\n";
$ret.='<link rel="stylesheet" href="'.$rep_themes.$theme.'popup.css" type="text/css" /> <!--media="screen"-->'."\n";

$ret.='<link rel="shortcut icon" href="favicon.ico" />'."\n";
$ret.='<link rel="icon" type="image/x-icon" href="favicon.ico" />'."\n";
$ret.='<link rel="icon" type="image/png" href="meshcal_icon16x16.png"  sizes="16x16" />'."\n";
$ret.='<link rel="icon" type="image/png" href="meshcal_icon32x32.png"  sizes="32x32" />'."\n";
$ret.='<link rel="icon" type="image/png" href="meshcal_icon64x64.png"  sizes="64x64" />'."\n";

$ret.='<link rel="stylesheet" href="'.$rep_cal.cal_css().'" type="text/css" />'."\n";
if($traiter_cal){ //si on doit afficher un calendrier...
	//... on met le jour actuel en valeur
	$ret.='<style type="text/css">#jour_'.(strtotime('today')+100).'{border: 2px solid red;background-color: rgb(255, 150, 150);}</style>'; /* A rendre configurable avec un fichier dans "themes/..." */
}
$ret.='</head>'."\n";
$ret.='<body>'."\n";




return $ret;
?>
