<?php
$ret="";
$test=false;

if(file_exists($rep_bases.'utilisateurs.csv')){$traiter_cal=true;}else{$traiter_cal=false;









/////////////////////////////////////////////////////////
/* traitment des info d'indentification du super-admin */
/////////////////////////////////////////////////////////

//Si pas de fichier mais requette déja faite

if(!file_exists($rep_bases.'utilisateurs.csv')
	&& isset($_POST["omega_prim"])){
	// teste de la validité des reponses
	$moa=true;

	if($_POST["omega_nom_sa"]==""){ // nom pas rempli
		$erreurs[]=array("message" => "Identifiant non renseigné.","en_cache" => false); //"Identifiant ou mot de passe incorecte !
		$affichage_erreur=true;
		$moa=false;
	}else{
		$omega_nom_sa= "SA#".$_POST["omega_nom_sa"]; //nom composé...
	}

	if(!verif_courriel($_POST["omega_mail_sa"])){
		$erreurs[]=array("message" => "Adresse mail non conforme.","en_cache" => false); //"Identifiant ou mot de passe incorecte !
		$affichage_erreur=true;
		$moa=false;
	}else{$courriel=$_POST["omega_mail_sa"];}

	if($_POST["omega_mail_serveur"]!=""){ // s'il y a une adresse mail dédié au serveur on prend celle du SA
		$courriel=$_POST["omega_mail_serveur"];
	}

	if(!verif_courriel($courriel)){ // si adresse non valable
		$erreurs[]=array("message" => "Adresse mail non conforme.","en_cache" => false); //"Identifiant ou mot de passe incorecte !
		$affichage_erreur=true;
		$moa=false;
	}else{	//si adresse valable... go !
		maj_config("courriel", $courriel);
	}

	/*TODO faire un truc avec $omega_mail_serveur */


	if($moa){
		//création du fichier temporaire
		//ligne de base
		$ligne=$omega_nom_sa."|"; //login SA
		$passw=nouveau_mot_de_pass_base();
		if($developement){
			echo "[log : ".$omega_nom_sa."]<br />";
			echo "[pass : ".$passw."]<hr />";
		}
		$ligne.=hash("sha256",$passw)."|"; //mot de passe chifré
		$ligne.=$_POST["omega_mail_sa"]."|"; //adresse courriel
		$ligne.=("0|");	//niveau d'autorisation maximum (inéfacable)
		$ligne.="|"; //contien normalement les autorisation par calendrier (inutil ici)
		$ligne.=$langue."|"; //langue
		$ligne.="non|"; //[6] pas de calendrier perso par defaut
		$ligne.="Super-administrateur"; //comentaire

		//création du ficher d'utilisateur proprement dit avec nom temporaire
		file_put_contents ( $rep_bases.'utilisateurs_temp.csv', $ligne);

		// envoi un courriel spécifique d'information de connexion.
		courriel_info_connexion($omega_nom_sa,$passw,$_POST["omega_mail_sa"],$langue); /*** lange prend l'adresse courriel !!! */

	}else{
		//on remet le formulaire
		unset($_POST["omega_prim"]);
	}

}







if(file_exists($rep_bases.'utilisateurs_temp.csv')
	&& isset($_POST["omega_final"])){
	
	//récuperation des info dans le fichier temporaire
	$ligne=file_get_contents( $rep_bases.'utilisateurs_temp.csv');
	if($developement){
		echo $ligne."<br />";
		echo hash("sha256",$_POST["pas"]);
	}
	$tab=explode("|",$ligne);

	//comparaison...
	if($_POST["log"]== $tab[0] && hash("sha256",$_POST["pas"])== $tab[1]){

		//renomer fichier temps en nom final
		rename ($rep_bases.'utilisateurs_temp.csv', $rep_bases.'utilisateurs.csv');

		//autoriser affichage de l'agenda
		$traiter_cal=true;


	}else{
		//Message d'erreur
		$erreurs[]=array("message" => "000 Les information de connexion en super-admin ne corespondent pas !","en_cache" => false); //"Identifiant ou mot de passe incorecte !
		$affichage_erreur=true;
	}
}











/////////////////////////////////
/* formulaire de renseignement */
/////////////////////////////////

// si pas de fichier ni requette ou si relance du procéssus
if((!file_exists($rep_bases.'utilisateurs.csv')
	&& !file_exists($rep_bases.'utilisateurs_temp.csv'))
	/*securité : interdir la procédure si le fichier final est déja créé !!!
	(sinon nimporte qui qui bidouille la requette http peux simuler un reinit et
	S'aproprier les droits super-admin...)*/
	|| (file_exists($rep_bases.'utilisateurs_temp.csv') && isset($_POST["omega_reinit"]))){ 
	$ret.='<div style="text-align: center;padding:200px 0"><form method="POST" action="" name="form">';

	//petit message d'intro...
	$ret.="Pour activer MeshCal, vous devez créer un compte super-administrateur.";
	$ret.=info("Le compte super utilisateur a la particularité de ne pouvoir etre effacé par persone.");
	$ret.="<br /><br />";

	//nom pour le super-admin
	$ret.='Identifiant du super-administateur : ';
	$ret.='<br />SA#<input name="omega_nom_sa" size="20" type="text" value="';
		$ret.=(isset($_POST["omega_nom_sa"])?$_POST["omega_nom_sa"]:"");
	$ret.='" /><br /><br />';

	//couriel du super-admin
	$ret.='Adresse courriel du super-administateur : ';
	$ret.='<br /><input name="omega_mail_sa" size="20" type="text" value="';
		$ret.=(isset($_POST["omega_mail_sa"])?$_POST["omega_mail_sa"]:"");
	$ret.='" /><br /><br />';

	//adresse dédié au serveur (si non vide)
	$ret.='Adresse courriel du serveur MeshCal : ';
	$ret.=info("Si vous avez créer une adresse spécialement pour votre serveur MeshCal, mettez là ici.<br :>
				Si vous laissez le champ vide, l'adresse utilisé sera celle du super_administrateur.");
	$ret.='<br /><input name="omega_mail_serveur" size="20" type="text" value="';
		$ret.=(isset($_POST["omega_mail_serveur"])?$_POST["omega_mail_serveur"]:"");
	$ret.='" /><br /><br />';

	//bouton et marqueur
	$ret.='<input name="omega_prim" value="" type="hidden">';
	$ret.=' <input type="submit" class="button" value="Activer..." />';
	$ret.=info("Après avoir validé, vous recevrez les informations de connexion du super-admin par mail.");
	
	$ret.='</form></div>';
}








///////////////////////////////////////////////////////////////////
/* Formulaire de première connexion (ou de relance du processus) */
///////////////////////////////////////////////////////////////////

// si l'envoi du mot de passe est fait (donc, si le fichier temporaire existe)
if(file_exists($rep_bases.'utilisateurs_temp.csv')){

	$ret.='<div style="text-align: center;padding:200px 0">';

	$ret.='<form method="post" action="">';

	//login
	$ret.="Entrez l'identifian super-admin : ";
	$ret.=info('vous devirez avoir reçu ces information par mail...<br />Le nom du super-admin commence par "SA#".')."<br />";
	$ret.='<input type="text" name="log" size="22"/><br /><br />';

	
	$ret.='Et le mot de passe :<br />';
	$ret.='<input type="password" name="pas" size="12" /><br /><br />';

	//bouton et marqueur
	$ret.='<input name="omega_final" value="" type="hidden"><br />';
	$ret.='<input type="submit" class="button" value="Entrer">';
	$ret.='</form>';

	//--------------------------------------------------------------------------
	$ret.='<br /><hr /><br />';

	//reinit...	
	$ret.='<form method="post" action="">';

	$ret.='<div style="text-align:left;margin:0 auto;width:380px">'; //pour faire style...
	$ret.="S'il y à eu un problème avec l'envoi de mail, vous pouvez retenter la procédure :</div>";

	//bouton et marqueur
	$ret.='<input name="omega_reinit" value="" type="hidden"><br />';
	$ret.='<input type="submit" class="button" value="Re-initialiser">';

	$ret.='</form></div>';
}










}return $ret;
?>
