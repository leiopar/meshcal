<?php
// https://meshcal.net

// CC BY SA - Jérôme Leignadier-Paradon (2015)
// Creative Commons 4.0
// http://creativecommons.org/licenses/by-sa/4.0/


/*	Afero GPL v3
	MeshCal - Calendar server (whis rfc 2445 and 5545)
    Copyright (C) 2015 Jérôme Leignadier-Paradon

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see :
	https://www.gnu.org/licenses/agpl.html
*/

// CeCILL 2.1 - Jérôme Leignadier-Paradon (2015)
// http://www.cecill.info/faq.fr.html

//------------------------------------------------------------------------------


 // routine d'inscription de nouveau utilisateur dans la base



//// traitement... ////

$nb_lignes_interface=1;
while (isset($_POST[($nb_lignes_interface+1)."identifiant"]))$nb_lignes_interface++;
//$liste_aff=list_aff2();/***TODO***/ //$metadonees_calendriers
$liste_lang=array("fr"); /***TODO***/ //$liste_lang=list_lang();
$i=0;
$table_entrees = array();
$table_inscrits = array();
$erreur = array();
$tab_1d= array();

if(isset($_POST["1identifiant"])){// aquisition des informations...
	for ($nb=1; $nb<=$nb_lignes_interface; $nb++){ //remplissage depuis formulaire
		$tab_1d[0]=trim($_POST[$nb.'identifiant']); /* identifiant */
		//$tab_1d[1]=trim($_POST[$nb.'nom']);			/***/
		$tab_1d['courriel']=trim($_POST[$nb.'courriel']);
		$tab_1d["lg"]=trim($_POST[$nb.'langue']);
		//$tab_1d['cal_modif']=trim($_POST[$nb.'calendriers']); /* calendrier */
		$tab_1d['info_divers']=trim($_POST[$nb.'infodiv']);
		$table_entrees[]=$tab_1d;
	}

	if (isset($_FILES['fichier']['tmp_name'])){// recuperation des info de fichier "csv" uploadé...
		if($_FILES['fichier']['tmp_name']!="")if($file = fopen($_FILES['fichier']['tmp_name'],"r")){//test présence fichier
			/* que deviennent les fichier 'tmp_name' aprés ??? */
			for($i=1;$i<=7;$i++){//permet d'ajouter 6 ligne de commentaire avent la liste proprement dite.
				$entete=fgets($file);
				
				$separateur_csv="";
				$position_sep1=0;
				$position_sep2=0;
				
				$position_sep1=strpos($entete, "]");
				$position_sep2=strpos($entete, "[",$position_sep1+1);
				if ($position_sep2-$position_sep1==2) $separateur_csv=substr($entete,$position_sep1+1,1);
				if ($position_sep2-$position_sep1==4) $separateur_csv=substr($entete,$position_sep1+2,1);
				if ($separateur_csv!="")break;
			}

			
			if ($separateur_csv!=""){
				while(! feof($file)){
					$tab_1d=(fgetcsv($file,0,$separateur_csv));
					if (count($tab_1d)>1){
						foreach($tab_1d as &$j){$j=trim($j,"\"' ");}unset($j);
					//foreach($tab_1d as $k => $j){$tab_1d[$k]=trim($tab_1d[$k],"\"' ");}
						if (isset($tab_1d[0])) if (count($tab_1d)>1) if(!($tab_1d[0]==""&&$tab_1d[1]=="")){//test ligne vide &&$tab_1d[1]==""
							$tab_1d["a"]="";$tab_1d["l"]="";
							$table_entrees[]=$tab_1d;
						}
					}
				}
			}else print (message(101)); // message rouge "Fichier invalide, utiliser le gabarit..."
		fclose($file);}
	}
	$tab_1d= array();
}

if(count($table_entrees)>0){//chargement de la base en memoire... control de validité des entrées




	foreach($table_entrees as &$tab_1d){//verification conformité et traitement
		$tab_1d["e"]="";
		if($tab_1d[0]=="")$tab_1d["e"].="77,"; // pas d'identifiant renseigné
		if ($tab_1d["e"]==""){
			$log=$tab_1d[0]; //nouvel_identifiant($tab_1d[0],$tab_1d[1]); /***/
		}else{}
		if ($tab_1d['courriel']!=""){
			if(!verif_courriel($tab_1d['courriel']))$tab_1d["e"].="92,"; //Adresse courriel non valide
		}else $tab_1d["e"].="94,"; //Pas d'adresse courriel renseigné
			
		//if($tab_1d[2]=="")$tab_1d["e"].="95,"; //Pas de nom d'calendrier renseigné
		



	/* a rasembler avec la fonction de reconstruction d'utilisateur ? */
		if ($tab_1d["e"]==""){// génération de la ligne à ajouter dans la base
			
			$ligne=$log."|"; //[0]
			$passw=nouveau_mot_de_pass_base();
			$ligne.=hash("sha256",$passw)."|"; //[1]
			$ligne.=$tab_1d['courriel']."|"; //[2]
			$ligne.=("5|");	//[3]niveau d'autorisation par defaut
			$ligne.="|"; //$ligne.=$tab_1d['cal_modif']."|"; //[4]
			if (in_array($tab_1d["lg"],$liste_lang)){//traitement info langue
				$ligne.=$tab_1d["lg"]."|"; //[5]
			}else{
				$tab_1d["e"].="96,"; //Pas de langue deffini
				$tab_1d["lg"]="";
			}
			//$ligne.=date("d/m/y")."|";
			
			//for($j=2;$j<=count($tab_1d)-4;$j++)
			//	$ligne.=$tab_1d[$j]."|";//insertion d'éventuel autres information...

			$ligne.="non|"; //[6] pas de calendrier perso par defaut
			$ligne.=$tab_1d["info_divers"]; //[7]

			//if (in_array($tab_1d["cal_modif"],$metadonees_calendriers)){
			//	$ligne.=$tab_1d["cal_modif"];
			//}else $tab_1d["cal_modif"]=""; /***/ //control si le nom du calendrier n'a pas été forcé de facon inaproprié.
		}
		//echo "(erreur : ".$tab_1d["e"].")";
		if($tab_1d["e"]==""){ //traitement des entrés valides
			ajout_dans_fic($ligne,$rep_bases.'utilisateurs.csv'); //ajout de la ligne à la base général d'utilisateur
			//function courriel_info_conexion($login,$password,$adrcourriel,$langue)
			// envoi un courriel spécifique d'information de connexion (dans la langue du nouvel inscrit)
			courriel_info_connexion($log,$passw,$tab_1d['courriel'],$tab_1d["lg"]); /*** lange prend l'adresse courriel !!! */
			
			//message standard(vert) de confirmation pour inscripteur
			$information[]=array(message(102,array("nom" => $log)),"vert");/** creer popup de message */
			//if($tab_1d["cal_modif"]!=""){
			//	inscr_dans_aff($log,$tab_1d[2],$tab_1d["cal_modif"],$tab_1d["lg"]); //inscription du nouvel utilisateur dans le calendrier demandé 
			//}
			
			$img_utilisateurs[]=array('log'=>$log, 'cou'=>$tab_1d['courriel']);
		}else if($tab_1d["e"]!="77,94,") $table_inscrits[]=$tab_1d;//si entrée incorect (et non vide) : rebelote...
	}
}
$tab_1d= array();// création d'une ligne vide pour permetre une entrée manuelle.
for($i=0;$i<5;$i++) $tab_1d[]="";

$tab_1d[0]="";
$tab_1d['courriel']="";
$tab_1d["lg"]="";
$tab_1d["cal_modif"]="";
$tab_1d["info_divers"]="";
$tab_1d["e"]="";
$table_inscrits[]=$tab_1d;







 //// Génération du tableau HTML d'entrées et de correction ////
//--------------------------------------------------------------------------------

$nb_inscrit=count($table_inscrits);
// script de remplissage auto de "calendrier"
/*

code vestigiel à purger

$ret1=<<<EOT
<script type="text/javascript">
<!--
function aff_presel(){
	var moa=document.getElementById("calendriers").value;
	if(moa!=""){
		for (i=1;i<=${nb_inscrit};i++)document.getElementById(i+"calendrier").value=moa;
		if(moa==0) document.getElementById("calendriers").value="";
	}
}
-->
</script>

EOT;*/

$ret1='<form method="post" action="" enctype="multipart/form-data" >';
$ret1.='<input name="admin" value="inscription_nouvel_utilisateur" type="hidden">';





// ligne de titre
$ret1.="
<input type='hidden' name='nb_inscrit' value='${nb_inscrit}'>
	<table>
		<tr>
			<td>&nbsp;</td>";
			$ret1.="<td>".message(79)."</td>"; //identifiant
			$ret1.="<td>".message(83)."</td>"; //Courriel
			$ret1.="<td>".message(84)."</td>"; //Langue
			//$ret1.="<td>calendrier's)";
			//$ret1.=info("Ici, metre les numéros des calendriers (séparrés par des point-virgules) ou l'utilisateur sera autorisé à ecrire.<br>Liste :<br />".
			//		list_calendriers_html());
			//$ret1.="</td>"; //calendrier
			$ret1.="<td>".message(87)."</td>";	//info divers
		$ret1.="</tr>";

		$nb=0;
// ligne de formulaire préremplis...
foreach ($table_inscrits as $tab_1d){
	$nb++;
	$ret1.="
		<tr>
			<td>${nb}</td>
			<td><input type=text name='${nb}identifiant'  value='${tab_1d[0]}' size='12' /></td>".
			//<td><input type=text name='${nb}nom' value='${tab_1d[1]}' size='12' /></td>
			//"<td><input type=text name='${nb}calendrier'  value='${tab_1d[3]}' size='12' /></td>
			"<td><input type=text name='${nb}courriel'  value='${tab_1d['courriel']}' size='25' /></td>
			<td>
				<select name='${nb}langue' id='${nb}langue'  style='width:100%;text-align:center'>";

	$l=($tab_1d["lg"]==""?$langue:$tab_1d["lg"]); //si pas de langue déffini alors préremplissage de proposition avec langue par défaut.
	foreach($liste_lang as $j){
		$ret1.=('<option '.($l==$j?"selected ":"").'value="'.$j.'">'.$j.'</option>'."\n");
	}

	$ret1.="
					</select>
				</td>";
	
	$ret1.="	<td><input type=text name='${nb}infodiv'  value='${tab_1d['info_divers']}' size='20' /></td>
			</tr>";

	if ($tab_1d["e"]!=""){// affichage d'erreur...
		$ret1.="<tr><td colspan='6'>";
		$ret1.=compil_liste_erreur($tab_1d["e"]);
		$ret1.="</td></tr>";
		/* emplacement pour surchoix contextuel...*/
	}
}

// fin de tableau et bouton de soumition...
//$moa=file_exists("public/charrette/langues/".$_SESSION['lng']."/gabarit_inscriptions.csv")?$_SESSION['lng']:$langue;
$ret1.="
	</table>"
	//."<a href='public/charrette/langues/".$moa."/gabarit_inscriptions.csv'>".message(100)."(csv)</a> <input name='fichier' type='file' accept='text/csv'/>
	//<a href='gabarit_inscriptions.csv'>".message(100)."(csv)</a> <input name='fichier' type='file' accept='text/csv'/>"
."<br/>
	<input type='submit' name='Submit' value='lancer inscription...' />
</form>";


return $ret1;
?>
